<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Helpers\notif;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpWord\TemplateProcessor;

class SuratKeluarController extends Controller
{
    public function index()
    {
        // dd(Auth::user()->jabatan_id);
        $dbe = DB::table('tm_jabatan')
            ->select('*')
            ->get();
        $generate = ($this->auto_generate_id());
        $notif = notif::notifikasi();
        $notif_surat_keluar = notif::notifikasi_surat_keluar();
        DB::table('tc_suratkeluar')
        ->where('id_jabatan', Auth::user()->jabatan_id)
        ->update(['read_surat' => 1]);
        return view ('suratkeluar.index', compact('notif', 'generate', 'dbe', 'notif_surat_keluar'));
    }

    public function AjaxDetail($id_surat)
    {
        $suratmasuk = \DB::table('tt_suratkeluar as sm')
            ->Leftjoin('tm_jenis_surat as js', 'sm.id_jenis', 'js.id_jenis')
            ->Leftjoin('tm_jabatan as ja', 'sm.tembusan', 'ja.id_jabatan')
            ->select('*')
            ->where('sm.id_suratkel', $id_surat)
            ->first();
        // dd($suratmasuk);
        return response()->json(['status'=> 'success', 'result'=> $suratmasuk], 200);

    }

    public function show_datatable(){
        try {
            $result = [];
            $count = 1;
            if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3 || Auth::user()->role_id == 4){
                $query = DB::table('tt_suratkeluar')
                    ->Leftjoin('tm_jenis_surat', 'tt_suratkeluar.id_jenis', 'tm_jenis_surat.id_jenis')
                    ->Leftjoin('users', 'tt_suratkeluar.user_id', 'users.id')

                    ->orderBy('tt_suratkeluar.created_at', 'desc')
                    ->get();
            }else{
                $query = DB::table('tt_suratkeluar')
                ->Leftjoin('tm_jenis_surat', 'tt_suratkeluar.id_jenis', 'tm_jenis_surat.id_jenis')
                ->leftjoin('tc_suratkeluar', 'tt_suratkeluar.id_suratkel', 'tc_suratkeluar.id_suratkel')
                ->leftjoin('users', 'tt_suratkeluar.user_id', 'users.id')
                ->where('user_id', Auth::user()->id)
                ->orwhere('id_jabatan', Auth::user()->jabatan_id)
                ->groupBy('tt_suratkeluar.id_suratkel')
                ->orderBy('tt_suratkeluar.created_at', 'desc')
                ->get();
            }
            // <a href="#" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon"
            // data-toggle="modal"
            // data-suratid= "'. $suratkeluar->id_suratkel.'"
            // data-target="#modal-edit" id="btn_update_surat">
            // <span>
            //     <i class="la la-edit"></i>
            //     <span>Ubah</span>
            // </span>
            // </a>


            // dd($query);
            foreach ($query as $suratkeluar) {
                if (Auth::user()->role_id == 1 || Auth::user()->role_id == 4 || Auth::user()->role_id == 3){
                    $action_edit = '<center>

                    <a href="#" class="btn btn-danger m-btn btn-sm m-btn m-btn--icon" id="btn-delete-surat-masuk"
                    data-id="' . $suratkeluar->id_suratkel . '">
                    <span>
                        <i class="la la-trash"></i>
                        <span>Hapus</span>
                    </span>
                    </a>';
                }
                // Auth::user()->jabatan_id == $suratkeluar->user_id
                else if(Auth::user()->role_id == 2 && Auth::user()->jabatan_id == $suratkeluar->user_id){
                    $action_edit = '<center>
                     <a href="#" class="btn btn-danger m-btn btn-sm m-btn m-btn--icon" id="btn-delete-surat-masuk"
                    data-id="' . $suratkeluar->id_suratkel . '">
                    <span>
                        <i class="la la-trash"></i>
                        <span>Hapus</span>
                    </span>
                    </a>';
                }else{
                    $action_edit = '<center>';
                }


                $action_detail = ' <a href="#" class="btn btn-accent btn-sm m-btn  m-btn m-btn--icon"
                                data-toggle="modal"
                                data-suratiddetail= "'. $suratkeluar->id_suratkel.'"
                                data-target="#modal-detail" id="btn_detail_surat">
                                <span>
                                    <i class="la la-info"></i>
                                    <span>Detail</span>
                                </span>
                                </a></center>';

                $data = [];
                $data[] = $count++;
                $data[] ='<b>'.$suratkeluar->name.'</b>';
                $data[] = (date("d-m-Y", strtotime($suratkeluar->tgl_suratkel)));
                $data[] =$suratkeluar->nama;
                $data[] =$suratkeluar->nomor;
                $data[] =$suratkeluar->perihal;

                $data[] = $action_edit.' '.$action_detail ;
                $result[] = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function auto_generate_id(){
        // $query = DB::select('select max(id_suratkel) as maxkode from tt_suratkeluar')->first();
        $query = DB::table('tt_suratkeluar')->select(DB::raw('max(id_suratkel) as maxkode'))->first();
        $kode = $query->maxkode;

        $increment = (int) substr($kode, 0, 10);
        $increment++;
        // dd($increment);
        $char = "";
        $Kodekode = $char . sprintf("%1s", $increment);
        // dd($Kodekode);
        return $Kodekode;
    }

    public function check_format_surat($id){
        $query = DB::table('tm_format_surat_keluar')
                ->where('id', $id)
                ->first();
        echo json_encode($query);
    }

    public function simpan(Request $request){
        // $array = ['']
        $db = DB::table('tm_jabatan')
                ->select('nama_jabatan')
                ->whereIn('id_jabatan', $request->tembusan)->get();
        // $ok = explode(',' , $db);
        // $ok = implode(',', $request->all());
        $arr = [];
        $a = 0;
        foreach($db as $key => $val){
            $a++;
            array_push($arr, '<w:br /> '.$a.'. '.$val->nama_jabatan);
        }
        $tes = implode(",",$arr);
        // dd($tes);
        $validator = \Validator::make($request->all(), [
            'tgl_suratkel' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $reverse_tgl_suratkel_word = date("d-M-Y", strtotime($request->tgl_suratkel));
        $format_surat_file = 'FORMATSURAT/'.$request->format_surat;
        $templateProcessor = new TemplateProcessor($format_surat_file);
        $templateProcessor->setValue('nomorsurat', $request->nomor);
        $templateProcessor->setValue('perihal', $request->perihal);
        $templateProcessor->setValue('tgl_surat', $reverse_tgl_suratkel_word);
        $templateProcessor->setValue('tembusan', $tes);
        $date = date('d-m-Y');
        $templateProcessor->saveAs('FILESURATKELUAR/'.'Surat Keluar - '.$request->nomor.'.docx');

        $reverse_tgl_suratkel = date("Y-m-d", strtotime($request->tgl_suratkel));
        DB::table('tt_suratkeluar')->insert([
                            'id_suratkel'   => $this->auto_generate_id(),
                            'tgl_suratkel'  => $reverse_tgl_suratkel,
                            'id_jenis'           => $request->id_jenis,
                            'nomor'              => $request->nomor,
                            'perihal'            => $request->perihal,
                            'kepada'             => $request->kepada,
                            'tembusan'           => implode(',',$request->tembusan),
                            'file_upload'        => 'Surat Keluar - '.$request->nomor.'.docx',
                            'user_id'            => $request->auth_user,
                            'created_at'         => \Carbon\Carbon::now()
                ]);

        $input_double =  count($request->tembusan);
        for ($a = 0; $a < $input_double; $a++){
            $data2 = array(
                'id_suratkel'		=> $request->id_suratkel,
                'id_jabatan'		=> $request->tembusan[$a],
        );
        DB::table('tc_suratkeluar')->insert($data2);
        }

        // dd($query);
        return response()->json(['success'=>'Data berhasil ditambahkan']);
    }

    public function destroy(Request $request)
    {
        try {
           DB::table('tt_suratkeluar')->where('id_suratkel', '=', $request->id)->delete();
        DB::table('tc_suratkeluar')->where('id_suratkel', '=', $request->id)->delete();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 404);
        }
        return response()->json(['status' => 'success', 'result' => 'Data berhasil dihapus'], 200);
    }

    public function update(Request $request)
    {
       // dd($request->all());
       $db = DB::table('tm_jabatan')
                ->select('nama_jabatan')
                ->whereIn('id_jabatan', $request->tembusan)->get();
        // $ok = explode(',' , $db);
        // $ok = implode(',', $request->all());
        $arr = [];
        $a = 0;
        foreach($db as $key => $val){
            $a++;
            array_push($arr, '<w:br /> '.$a.'. '.$val->nama_jabatan);
        }
        $tes = implode(",",$arr);
       $query_check2 = \DB::table('tt_suratkeluar')->where('id_suratkel', $request->id_suratkel)->first();
       $request_file = $request->file_upload;
       $post = $request->all();
        // dd($request_file);
                    if ($request_file) {
                        if(\File::exists(trim(public_path('FILESURATKELUAR').'/'.$query_check2->file_upload))){
                                // unlink(trim(public_path('foto').'/'.$query_check->photo));
                                Storage::delete(trim(public_path('FILESURATKELUAR').'/'.$query_check2->file_upload));
                                $upload_pdf_jpg = 'Surat Keluar - '.$request->nomor.'docx';
                                $reverse_tgl_suratkel_word = date("d-M-Y", strtotime($request->tgl_suratkel));
                                $format_surat_file = 'FORMATSURAT/'.$request->format_surat;
                                $templateProcessor = new TemplateProcessor($format_surat_file);
                                $templateProcessor->setValue('nomorsurat', $request->nomor);
                                $templateProcessor->setValue('perihal', $request->perihal);
                                $templateProcessor->setValue('tgl_surat', $reverse_tgl_suratkel_word);
                                $templateProcessor->setValue('tembusan', $tes);
                        }
                    }


    DB::table('tc_suratkeluar')->where('id_suratkel', '=', $request->id_suratkel)->delete();
    $reverse_tgl_surat = date("Y-m-d", strtotime($request->tgl_suratkel));
    try {
        DB::table('tt_suratkeluar')->where('id_suratkel', $request->id_suratkel)
                                  ->update([

                                        'tgl_suratkel'  => $reverse_tgl_surat,
                                        'id_jenis' => $request->id_jenis,
                                        'nomor'           => $request->nomor,
                                        'kepada'                => $request->kepada,
                                        'file_upload'           => $upload_db,
                                        'user_id'               => Auth::user()->id,
                                        'tembusan'              => implode(',',$request->tembusan),
                                        'updated_at'            => \Carbon\Carbon::now()
                                    ]);
        // DB::table('tc_suratkeluar')->where('id_suratkel', '=', $request->id_suratkel)->delete();
        $input_double =  count($request->tembusan);
        for ($a = 0; $a < $input_double; $a++){
            $data2 = array(
                'id_suratkel'		=> $request->id_suratkel,
                'id_jabatan'		=> $request->tembusan[$a],
        );
        DB::table('tc_suratkeluar')->insert($data2);
        }
        // dd($request->all());
        return response()->json(['status' => 'success', 'result' => 'Data berhasil diubah'], 200);
    } catch (\Exception $exception) {
        return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
    }
    }

    public function show_datatable_format_surat(){
        try {
            $result = [];
            $count = 1;
            $query = DB::table('tm_format_surat_keluar')
            ->get();

            // dd($query);
            foreach ($query as $format_surat) {
                $action_edit = '<center>
                                <a href="#" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon"
                                data-toggle="modal"
                                data-id= "'. $format_surat->id.'"
                                data-target="#modal-edit-format-surat" id="btn_update_format_surat">
                                <span>
                                    <i class="la la-edit"></i>
                                    <span>Ubah</span>
                                </span>
                                </a>';

                $action_detail = '<a href="#" class="btn btn-danger m-btn btn-sm m-btn m-btn--icon" id="btn-delete-format-surat"
                                data-id="' . $format_surat->id . '">
                                <span>
                                    <i class="la la-trash"></i>
                                    <span>Hapus</span>
                                </span>
                                </a></center>';

                $data = [];
                $data[] = $count++;
                $data[] ='<b style="color:#43568b">'.$format_surat->jenis_surat.'</b>';
                $data[] =$format_surat->keterangan;
                $data[] =$format_surat->file_surat;

                $data[] = $action_edit.' '.$action_detail ;
                $result[] = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function simpanFormatSurat(Request $request){
        if($request->file_upload_format_surat == null){
            $request->file_upload_format_surat == null;
        }else{
            $file_word = $request->file('file_upload_format_surat');

            if ($file_word) {
                $file = 'Format Surat - '.$request->jenis_surat. '.' . $file_word->getClientOriginalExtension();
                $filepath = public_path('FORMATSURAT');
                if (!is_dir($filepath)) {
                    \File::makeDirectory(public_path('FORMATSURAT'));
                }
                $upload = $file_word->move($filepath, $file);
                if ($upload) {
                    $request->file_upload_format_surat = $file;
                }
            } else {
                $request->file_upload_format_surat = null;
            }
        }

        DB::table('tm_format_surat_keluar')->insert([
            'jenis_surat'           => $request->jenis_surat,
            'keterangan'              => $request->keterangan,
            'file_surat'        => $request->file_upload_format_surat,
            'created_at'         => \Carbon\Carbon::now()
        ]);

        return response()->json(['success'=>'Format Surat berhasil ditambahkan']);
    }

    public function AjaxDetailFormatSurat($id_surat)
    {
        $suratkeluarformat = \DB::table('tm_format_surat_keluar as fs')
            ->select('*')
            ->where('fs.id', $id_surat)
            ->first();
        // dd($suratkeluarformat);
        return response()->json(['status'=> 'success', 'result'=> $suratkeluarformat], 200);

    }

    public function update_format_surat(Request $request)
    {

    $query_check = \DB::table('tm_format_surat_keluar')->where('id', $request->idformatsurat)->first();
    $doc = $request->file('file_upload_format_surat');
    // dd($doc);
    if ($doc) {
        if(File::exists(trim(public_path('FORMATSURAT').'/'.$query_check->file_surat))){
                // unlink(trim(public_path('file_surat').'/'.$query_check->file_surat));
                Storage::delete(trim(public_path('FORMATSURAT').'/'.$query_check->file_surat));
        }

        $format_surat = 'Format Surat - '.$request->jenis_surat. '.' . $doc->getClientOriginalExtension();

        $filepath = public_path('FORMATSURAT');
        if (!is_dir($filepath)) {
            \File::makeDirectory(public_path('FORMATSURAT'));
        }
        $upload = $doc->move($filepath, $format_surat);
        if ($upload) {
            $upload_db = $format_surat;
        }
    } else {
        $upload_db = $query_check->file_surat;
    }


    try {
        DB::table('tm_format_surat_keluar')->where('id', $request->idformatsurat)
                                  ->update([
                                        'jenis_surat'  => $request->jenis_surat,
                                        'keterangan' => $request->keterangan,
                                        'file_surat'           => $upload_db,
                                        'updated_at'            => \Carbon\Carbon::now()
                                    ]);
        // dd($request->all());
        return response()->json(['status' => 'success', 'result' => 'Data berhasil diubah'], 200);
    } catch (\Exception $exception) {
        return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
    }
    }

    public function destroy_format_surat(Request $request)
    {
        try {
           DB::table('tm_format_surat_keluar')->where('id', '=', $request->id)->delete();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 404);
        }
        return response()->json(['status' => 'success', 'result' => 'Format Surat berhasil dihapus'], 200);
    }
}
