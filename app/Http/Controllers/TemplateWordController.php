<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpOffice\PhpWord\TemplateProcessor;
use Exception;
class TemplateWordController extends Controller
{
    public function index()
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $templateProcessor = new TemplateProcessor('Template.docx');
        $templateProcessor->setValue('firstname', 'John');
        $templateProcessor->setValue('lastname', 'Doe');

       $templateProcessor->saveAs('test32.docx');
        // dd($templateProcessor);
    }
}
