<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class SelectController extends Controller
{
    public function loadjenissurat(Request $request)
    {
        if ($request->has('q')) {
            $cari = $request->q;
            $data =DB::table('tm_jenis_surat')->select('id_jenis', 'nama')->where('nama', 'LIKE', '%'.$cari.'%')->get();
            return response()->json($data);
        }else{
            $data = DB::table('tm_jenis_surat')->select('id_jenis', 'nama')->get();
            return response()->json($data);
        }
    }

    public function loadkategorisurat(Request $request)
    {
        if ($request->has('q')) {
            $cari = $request->q;
            $data =DB::table('tm_kategori')->select('id_kategori', 'kategori')->where('kategori', 'LIKE', '%'.$cari.'%')->get();
            return response()->json($data);
        }else{
            $data = DB::table('tm_kategori')->select('id_kategori', 'kategori')->get();
            return response()->json($data);
        }
    }

    public function loadjabatan(Request $request)
    {
        if ($request->has('q')) {
            $cari = $request->q;
            $data =DB::table('tm_jabatan')->select('id_jabatan', 'nama_jabatan')->where('nama_jabatan', 'LIKE', '%'.$cari.'%')->get();
            return response()->json($data);
        }else{
            $data = DB::table('tm_jabatan')->select('id_jabatan', 'nama_jabatan')->get();
            return response()->json($data);
        }
    }

    public function loadjabatanwithauth(Request $request)
    {
        if ($request->has('q')) {
            $cari = $request->q;
            $data =DB::table('tm_jabatan')->select('id_jabatan', 'nama_jabatan')
                    ->where('nama_jabatan', 'LIKE', '%'.$cari.'%')
                    ->where('id_jabatan','!=',Auth::user()->jabatan_id)
                    ->get();
            return response()->json($data);
        }else{
            $data = DB::table('tm_jabatan')
                    ->where('id_jabatan','!=',Auth::user()->jabatan_id)
                    ->select('id_jabatan', 'nama_jabatan')
                    ->get();
            return response()->json($data);
        }
    }

    public function loadkodearsip(Request $request)
    {
        if ($request->has('q')) {
            $cari = $request->q;
            $data =DB::table('tm_kode_arsip')->select('id_kode_arsip', 'kode_arsip', 'keterangan')
                    ->where('keterangan', 'LIKE', '%'.$cari.'%')
                    ->orwhere('kode_arsip', 'LIKE', '%'.$cari.'%')
                    ->orderBy('kode_arsip', 'asc')
                    ->get();
            return response()->json($data);
        }else{
            $data = DB::table('tm_kode_arsip')->select('id_kode_arsip', 'kode_arsip', 'keterangan')
                        ->orderBy('kode_arsip', 'asc')
                        ->get();
            return response()->json($data);
        }
    }

    public function loadformatsurat(Request $request)
    {
        if ($request->has('q')) {
            $cari = $request->q;
            $data =DB::table('tm_format_surat_keluar')->select('id', 'jenis_surat')
                    ->where('jenis_surat', 'LIKE', '%'.$cari.'%')
                    ->orderBy('jenis_surat', 'asc')
                    ->get();
            return response()->json($data);
        }else{
            $data = DB::table('tm_format_surat_keluar')->select('id', 'jenis_surat')
                        ->orderBy('jenis_surat', 'asc')
                        ->get();
            return response()->json($data);
        }
    }
}
