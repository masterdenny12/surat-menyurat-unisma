<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Helpers\notif;

class MonitoringController extends Controller
{

    public function index()
    {
        $notif = notif::notifikasi();
        $notif_surat_keluar = notif::notifikasi_surat_keluar();
        return view('monitoring.index', compact('notif','notif_surat_keluar'));
    }

    public function show_datatable(){
        try {
            $result = [];
            $count = 1;
            $auth_role = Auth::user()->role_id;
            if($auth_role == 2){
                $where = 'where id_jabatan = '. Auth::user()->jabatan_id. ' and status_surat = 1';

            }else if($auth_role == 1){
                $where = '';
            }else if($auth_role == 4 || $auth_role == 3){
                $where = '';
            }

            $query = DB::select('select * from tt_suratmasuk LEFT JOIN tm_jenis_surat ON tt_suratmasuk.id_jenis = tm_jenis_surat.id_jenis '. $where);
            // dd($query);
            foreach ($query as $suratmasuk) {
                $check_button_action = Auth::user()->role_id;
                if($check_button_action == 1 || $check_button_action == 4){
                    $show_button_edit = '<div align ="left"><a href="view_monitoring/'.$suratmasuk->id_surat.'" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon"
                 >

                    </a> ';
                    $show_button_approve = '';
                }else if($check_button_action == 3 && $suratmasuk->status_surat == 0){
                    $show_button_approve = '<a href="#" class="btn btn-primary m-btn btn-sm m-btn m-btn--icon" data-toggle="modal"
                    data-id= "'. $suratmasuk->id_surat.'"
                    data-nomor_surat= "'. $suratmasuk->nomor_surat.'"
                    data-target="#modal-proses" id="btn_proses_surat">
                    <span>
                        <i class="la la-send"></i>
                        <span>Cek Surat</span>
                    </span>
                    </a> ';

                }
                else{

                    $show_button_approve = '';
                }


                $action_detail ='<a href="view_monitoring/'.$suratmasuk->id_surat.'" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon"
                               >
                                <span>
                                    <i class="la la-laptop"></i>
                                    <span>Detail</span>
                                </span>
                                </a> '.$show_button_approve.'</center>';
                $status_surat = [0 => '<span style="width:70px" class="m-badge m-badge--primary"><b>Diproses</b></span>',
                                 1 => '<span style="width:70px" class="m-badge m-badge--success"><b>Disetujui</b></span>',
                                 2 => '<span style="width:70px" class="m-badge m-badge--danger"><b>Ditolak</b></span>'
                                ];

                $data = [];
                $data[] = $count++;
                $data[] = (date("d-m-Y", strtotime($suratmasuk->tgl_surat)));
                $data[] = $suratmasuk->nama == 'PENTING' ? '<b style="color:red">'.$suratmasuk->nama.'</b>' :$suratmasuk->nama ;
                $data[] = ($suratmasuk->nomor_surat == null ? '-' : $suratmasuk->nomor_surat);
                $data[] = ($suratmasuk->perihal == null ? '-' : $suratmasuk->perihal);

                $data[] = $action_detail ;
                $result[] = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function view_monitoring($id_surat){
        $notif = notif::notifikasi();
        // dd($count_disposisi);
        $get_surat = DB::table('tt_suratmasuk as sm')
                    ->Leftjoin('tm_kategori as k', 'sm.id_kategori', 'k.id_kategori')
                    ->Leftjoin('tm_jenis_surat as js', 'sm.id_jenis', 'js.id_jenis')
                    ->Leftjoin('tm_jabatan', 'sm.id_jabatan', 'tm_jabatan.id_jabatan')
                    ->select('*')
                    ->where('id_surat', $id_surat)
                    ->first();
                    // dd($get_surat);
        $query = DB::table('tt_disposisi')
                ->select(
                    'tt_disposisi.id_disposisi',
                    'tt_disposisi.tgl_disposisi',
                    'tt_disposisi.id_jabatan as jabatan_disposisi',
                    'tt_disposisi.isi_disposisi',
                    'tt_disposisi.id_usrz as id_user_mendisposisi',
                    'tt_disposisi.opsi',
                    'tt_disposisi.jam_disposisi',
                    'a.nama_jabatan as jabatan_disposisi',
                    'b.nama_jabatan as jabatan_mendisposisi')
                ->where('id_surat', $id_surat)
                ->join('users', 'tt_disposisi.id_usrz','users.id')
                ->leftjoin('tm_jabatan as a', 'tt_disposisi.id_jabatan', 'a.id_jabatan')
                ->leftjoin('tm_jabatan as b', 'users.jabatan_id', 'b.id_jabatan')
                ->orderBy('tt_disposisi.created_at', 'desc')
                ->get();
        // dd($query);
        $arr = [];
        $a = [];
        $result = array();

        foreach($query as $key => $value){
            $result[$value->id_user_mendisposisi][] = $value ; //    array_push($a, [ 'id_usrz'       => $value->id_usrz,

        }
        $result = array_values($result);
        // dd($new);

        // dd($result);
        return view('monitoring.view_monitoring', compact('notif', 'get_surat', 'result'));
    }

    public function AjaxDetailMonitoring($id_surat, $id_jabatan, $id_usrz)
    {
        // dd(Auth::user()->jabatan_id);
            $detail_monitoring = \DB::table('tt_disposisi as sd')
                ->select('*')
                ->where('sd.id_surat', $id_surat)
                ->where('sd.id_jabatan', $id_jabatan)
                ->where('sd.id_usrz', $id_usrz)
                ->first();
        // dd($detail_monitoring);
        // dd($detail_monitoring);
        return response()->json(['status'=> 'success', 'result'=> $detail_monitoring], 200);

    }

}
