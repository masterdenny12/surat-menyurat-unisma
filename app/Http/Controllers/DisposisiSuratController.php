<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Helpers\notif;
use Illuminate\Support\Facades\Auth;

class DisposisiSuratController extends Controller
{
    public function index()
    {
        $notif = notif::notifikasi();
        // dd($update);
        $notif_surat_keluar = notif::notifikasi_surat_keluar();
        return view ('disposisi.index', compact('notif', 'notif_surat_keluar'));
    }
    public function show_datatable(){
        try {
            $result = [];
            $count = 1;
            $auth_role = Auth::user()->role_id;
            if($auth_role == 2){
                $where = 'where tt_suratmasuk.id_jabatan = '. Auth::user()->jabatan_id. ' and status_surat = 1';
                $where_disposisi = ' or tt_disposisi.id_jabatan = '.Auth::user()->jabatan_id;
            }else if($auth_role == 1){
                $where = '';
                $where_disposisi = '';
            }else if($auth_role == 4 || $auth_role == 3){
                $where = '';
                $where_disposisi = '';
            }

            $query = DB::select('select tt_suratmasuk.id_surat as id_surat_masuk,tt_suratmasuk.tgl_entry,
            tt_suratmasuk.tgl_surat, tm_jenis_surat.nama, tt_suratmasuk.nomor_surat, tt_suratmasuk.perihal,
            tt_suratmasuk.tgl_surat,tt_suratmasuk.id_jabatan, tt_suratmasuk.kepada, tt_disposisi.id_jabatan as jabatan_disposisi,
            tt_disposisi.id_usrz as user_input_disposisi
            from tt_suratmasuk
            LEFT JOIN tm_jenis_surat ON tt_suratmasuk.id_jenis = tm_jenis_surat.id_jenis
            LEFT JOIN tt_disposisi ON tt_suratmasuk.id_surat = tt_disposisi.id_surat
            LEFT JOIN users ON tt_disposisi.id_usrz = users.id
            '. $where. $where_disposisi. ' group by tt_suratmasuk.id_surat');
            // dd($query);
            foreach ($query as $suratmasuk) {
                $check_button_action = Auth::user()->role_id;
                if($check_button_action == 1 || $check_button_action == 2){
                    $show_button_edit = '<div align ="left">
                    <a href="view_disposisi/'.$suratmasuk->id_surat_masuk.'" class="btn btn-danger btn-sm m-btn  m-btn m-btn--icon">
                    <span>
                        <i class="la la-edit"></i>
                        <span>Disposisi</span>
                    </span>
                    </a>';
                }
                else{
                    $show_button_edit = '';
                }
                $action_edit = '<center>'. $show_button_edit.'
                                ';

                $status_surat = [0 => '<span style="width:70px" class="m-badge m-badge--primary"><b>Diproses</b></span>',
                                 1 => '<span style="width:70px" class="m-badge m-badge--success"><b>Disetujui</b></span>',
                                 2 => '<span style="width:70px" class="m-badge m-badge--danger"><b>Ditolak</b></span>'
                                ];
                if(Auth::user()->role_id == 1){
                    $check_disposisi = $suratmasuk->jabatan_disposisi != null ?
                    '<span style="width:70px" class="m-badge m-badge--primary"><b>Disposisi</b></span>' : '-';;
                }else if(Auth::user()->role_id == 2){
                    if($suratmasuk->user_input_disposisi == Auth::user()->id){
                        $check_disposisi = '<span style="width:110px" class="m-badge m-badge--success"><b>Mendisposisikan</b></span>';
                    }else{
                        $check_disposisi = $suratmasuk->jabatan_disposisi == Auth::user()->jabatan_id ?
                        '<span style="width:70px" class="m-badge m-badge--primary"><b>Disposisi</b></span>' : '-';
                    }
                }
                $data = [];
                $data[] = $count++;
                $data[] = (date("d-m-Y", strtotime($suratmasuk->tgl_surat)));
                $data[] = $suratmasuk->nama == 'PENTING' ? '<b style="color:red">'.$suratmasuk->nama.'</b>' :$suratmasuk->nama ;
                $data[] = ($suratmasuk->nomor_surat == null ? '-' : $suratmasuk->nomor_surat);
                $data[] = ($suratmasuk->perihal == null ? '-' : $suratmasuk->perihal);
                $data[] = $check_disposisi;
                // $data[] = $status_surat[($suratmasuk->status_surat)];

                $data[] = $action_edit ;
                $result[] = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function view_disposisi($id_surat){
        $notif = notif::notifikasi();
        if(Auth::user()->role_id == 1){
            $query = DB::table('tt_disposisi')
                ->select('*')
                ->join('tm_jabatan', 'tt_disposisi.id_jabatan', 'tm_jabatan.id_jabatan')
                ->join('tt_suratmasuk', 'tt_disposisi.id_surat', 'tt_suratmasuk.id_surat')
                ->join('users', 'tt_disposisi.id_usrz', 'users.id')
                ->where('tt_suratmasuk.id_surat', $id_surat)
                ->orderby('tt_disposisi.created_at', 'desc')
                ->get();
        }else{
            $query = DB::table('tt_disposisi')
            ->Leftjoin('tm_jabatan', 'tt_disposisi.id_jabatan', 'tm_jabatan.id_jabatan')
            ->Leftjoin('users', 'tt_disposisi.id_usrz', 'users.id')
            ->where(function($query)use($id_surat){
                $query->where(['tt_disposisi.id_surat' => $id_surat, 'tt_disposisi.id_jabatan' => Auth::user()->jabatan_id])
                ->select('*');
            })->orwhere(function($query)use($id_surat){
                $query->where(['tt_disposisi.id_usrz' =>  Auth::user()->id, 'tt_disposisi.id_surat' => $id_surat]);
            })->orderby('tt_disposisi.created_at', 'desc')->get();
        }
        // dd($query);

        $count_query = DB::table('tt_disposisi')
                ->select('*')
                ->Leftjoin('tm_jabatan', 'tt_disposisi.id_jabatan', 'tm_jabatan.id_jabatan')
                ->Leftjoin('users', 'tt_disposisi.id_usrz', 'users.id')
                ->where('id_surat', $id_surat)
                ->orwhere('tt_disposisi.id_usrz', Auth::user()->id)
                ->where('tt_disposisi.id_jabatan', Auth::user()->jabatan_id)
                ->count();
        $count_disposisi = DB::table('tt_disposisi')
                ->select('*')
                ->Leftjoin('tm_jabatan', 'tt_disposisi.id_jabatan', 'tm_jabatan.id_jabatan')
                ->Leftjoin('users', 'tt_disposisi.id_usrz', 'users.id')
                ->where('id_surat', $id_surat)
                ->where('tt_disposisi.id_usrz', Auth::user()->id)
                ->count();
        // dd($count_disposisi);
        $get_surat = DB::table('tt_suratmasuk as sm')
                    ->Leftjoin('tm_kategori as k', 'sm.id_kategori', 'k.id_kategori')
                    ->Leftjoin('tm_jenis_surat as js', 'sm.id_jenis', 'js.id_jenis')
                    ->Leftjoin('tm_jabatan', 'sm.id_jabatan', 'tm_jabatan.id_jabatan')
                    ->select('*')
                    ->where('id_surat', $id_surat)
                    ->first();


        return view('disposisi.view_disposisi', compact('query', 'notif', 'get_surat', 'count_query', 'count_disposisi'));
    }

    public function simpan(Request $request){
        $post = $request->all();
        $count = count($post['id_jabatan']);
        $opsi =implode(",", $post['opsi']);

        for($a = 0; $a < $count; $a++){
            DB::table('tt_disposisi')->insert([
                'id_surat'			=> $post['id_surat'],
                'tgl_disposisi'		=> date('Y-m-d'),
                'id_jabatan'        => $request->id_jabatan[$a],
                'isi_disposisi'     => $request->isi_disposisi,
                'jam_disposisi'		=> date('H:i:s', time()),
                'id_usrz'			=> Auth::user()->id,
                'opsi'              => $opsi,
                'created_at'        => \Carbon\Carbon::now()
            ]);
        }

        // dd($query);
        return response()->json(['success'=>'Data berhasil ditambahkan']);
    }
}
