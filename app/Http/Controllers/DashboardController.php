<?php

namespace App\Http\Controllers;

use App\Helpers\notif;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $notif = notif::notifikasi();
        $notif_surat_keluar = notif::notifikasi_surat_keluar();
        if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3 || Auth::user()->role_id == 4){
            $count_surat_masuk = DB::table('tt_suratmasuk')
                                ->count();
            $count_surat_keluar = DB::table('tt_suratkeluar')
                                ->count();
            $count_awal_disposisi = DB::table('tt_disposisi')
                                ->select('id_Surat')
                                ->groupBy('id_Surat')->get();

            $count_disposisi = count($count_awal_disposisi);
            $count_monitoring = DB::table('tt_disposisi')
                                ->select('id_Surat')
                                ->count();
            $str = '';
            $v_idusrz = '';

            // dd($count_disposisi);
        }else if(Auth::user()->role_id == 2){
            $count_surat_masuk = DB::table('tt_suratmasuk')
                                ->where('id_jabatan', Auth::user()->jabatan_id)
                                ->where('status_surat', 1)
                                ->count();
            $count_surat_keluar = DB::table('tt_suratkeluar')
                                ->Leftjoin('tm_jenis_surat', 'tt_suratkeluar.id_jenis', 'tm_jenis_surat.id_jenis')
                                ->leftjoin('tc_suratkeluar', 'tt_suratkeluar.id_suratkel', 'tc_suratkeluar.id_suratkel')
                                ->leftjoin('users', 'tt_suratkeluar.user_id', 'users.id')
                                ->where('user_id', Auth::user()->id)
                                ->orwhere('id_jabatan', Auth::user()->jabatan_id)
                                ->groupBy('tt_suratkeluar.id_suratkel')
                                ->get();

            $count_surat_keluar = count($count_surat_keluar);
            $count_disposisi = DB::table('tt_disposisi')
                                ->where('id_jabatan', Auth::user()->jabatan_id)
                                ->count();
            $count_monitoring = DB::table('tt_disposisi')
                                ->leftjoin('tt_suratmasuk', 'tt_disposisi.id_surat', 'tt_suratmasuk.id_surat')
                                ->select('*')
                                ->where('tt_suratmasuk.id_jabatan', Auth::user()->jabatan_id)
                                ->count();
            $str = ' and status_surat = 1';
            $v_idusrz = ' or id_jabatan = '.Auth::user()->jabatan_id;


            // dd($count_monitoring);
        }
        $tahun = DB::select('select year(`tgl_surat`)
        as tahun from tt_suratmasuk  where year(`tgl_surat`) '.$str.$v_idusrz .'
            group by tahun');
        $data = [];

        // dd($tahun);
        array_push($data,  [
            'total_surat_masuk' => $count_surat_masuk,
            'total_surat_keluar' => $count_surat_keluar,
            'total_surat_disposisi' => $count_disposisi,
            'count_monitoring' => $count_monitoring,
            ]);
        // dd($data);
        return view('dashboard.index',compact('data', 'notif', 'notif_surat_keluar', 'count_monitoring', 'tahun'));
    }

    public function graph_surat_masuk($tahun=null){
        $tahun = $tahun? $tahun : date('Y');
        $v_idjabatan 	= Auth::user()->jabatan_id;
        $lepel 			= Auth::user()->role_id;
        // pre($tahun,1);
        if($lepel == 1 || $lepel == 4 || $lepel == 3){
            $jabatan = '';
            $status = '';
        }else if($lepel == 2){
            $jabatan = ' and id_jabatan = "'.$v_idjabatan.'"';
            $status = ' and status_surat = 1';
        }
         $query = DB::select('select MONTHNAME(tgl_surat) AS bulan,MONTH(tgl_surat) AS bulan_number,
         COUNT(tgl_surat) AS jumlah FROM tt_suratmasuk WHERE YEAR(tgl_surat) = '.$tahun.$jabatan.$status.'
         GROUP BY bulan ORDER BY tgl_surat');
        // dd($query);
        //  var_dump($query);
         $data['bulan'] = [];
         $data['jumlah'] =[];
         foreach ($query as $key => $value) {
             array_push($data['bulan'], $value->bulan);
             array_push($data['jumlah'], (int)$value->jumlah);
         }

            // return json_encode($data);
         echo json_encode($data);


    }

    public function graph_surat_keluar($tahun=2020){
        $tahun = $tahun? $tahun : date('Y');
        $v_user_id = Auth::user()->id;
        $v_idjabatan 	= Auth::user()->jabatan_id;
        $lepel 			= Auth::user()->role_id;

        if($lepel == 1 || $lepel == 4 || $lepel == 3){
            $query = DB::select('SELECT  MONTHNAME(tgl_suratkel) AS bulan,MONTH(tgl_suratkel) AS bulan_number,
                COUNT(tgl_suratkel) AS jumlah FROM `tt_suratkeluar` WHERE YEAR(tgl_suratkel) = "'.$tahun.'"
                GROUP BY bulan ORDER BY tgl_suratkel');
        }
        else{
            $query =  DB::table('tt_suratkeluar')
                                ->leftjoin('tc_suratkeluar', 'tt_suratkeluar.id_suratkel', 'tc_suratkeluar.id_suratkel')
                                ->leftjoin('users', 'tt_suratkeluar.user_id', 'users.id')
                                ->where('user_id', Auth::user()->id)
                                ->whereYear('tgl_suratkel' ,$tahun)
                                ->orwhere('tc_suratkeluar.id_jabatan' , Auth::user()->jabatan_id)
                                ->orderBy('tgl_suratkel')
                                ->get();
            // dd(count($query));
            // dd(Auth::user()->jabatan_id);

            // for ($i = 1; $i <= 10; $i++) {
            //     echo $i;
            // };
            $a='';
            for($b=1;$b<=100;$b++){
                $a .= $b.',';
            }

            $c = '('.$b.')';
            $d = str_replace("100,","100",$a);
            // dd($d);
            $query = DB::select('SELECT  MONTHNAME(tgl_suratkel) AS bulan,MONTH(tgl_suratkel) AS bulan_number,
            COUNT(tgl_suratkel) AS jumlah FROM `tt_suratkeluar`
            WHERE YEAR(tgl_suratkel) = "'.$tahun.'" and user_id = "'.$v_user_id.'" or tembusan =
            4
            GROUP BY bulan ORDER BY tgl_suratkel');
        }
        // dd($query);

        //  var_dump($query);
         $data['bulan'] = [];
         $data['jumlah'] =[];
         foreach ($query as $key => $value) {
             array_push($data['bulan'], $value->bulan);
             array_push($data['jumlah'], (int)$value->jumlah);
         }


            // return json_encode($data);
         echo json_encode($data);


    }

}
