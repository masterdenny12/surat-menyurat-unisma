<?php

namespace App\Http\Controllers;

use App\Helpers\notif;
use Illuminate\Http\Request;
use DB;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use DateTime;
use Illuminate\Support\Facades\Storage;
use File;

class SuratMasukController extends Controller
{

    public function index()
    {
        $notif = notif::notifikasi();
        $notif_surat_keluar = notif::notifikasi_surat_keluar();
       DB::table('tt_suratmasuk')
                    ->where('id_jabatan', Auth::user()->jabatan_id)
                    ->where('status_surat',1)
                    ->update(['read_surat' => 1]);
        // dd($update);
        return view ('suratmasuk.index', compact('notif', 'notif_surat_keluar'));
    }
    public function show_datatable(){
        try {
            $result = [];
            $count = 1;
            $auth_role = Auth::user()->role_id;
            if($auth_role == 2){
                $where = 'where tt_suratmasuk.id_jabatan = '. Auth::user()->jabatan_id. ' and status_surat = 1 ';

            }else if($auth_role == 1){
                $where = '';
            }else if($auth_role == 4 || $auth_role == 3){
                $where = '';
            }

            $query = DB::select('select * from tt_suratmasuk
            LEFT JOIN tm_jenis_surat ON tt_suratmasuk.id_jenis = tm_jenis_surat.id_jenis
            '. $where. 'group by tt_suratmasuk.id_surat order by tt_suratmasuk.created_at desc');
            // dd($query);
            foreach ($query as $suratmasuk) {
                $check_button_action = Auth::user()->role_id;
                if($check_button_action == 1 || $check_button_action == 4){
                    $show_button_edit = '<div align ="left"><a href="#" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon"
                    data-toggle="modal"
                    data-suratid= "'. $suratmasuk->id_surat.'"
                    data-target="#modal-edit" id="btn_update_surat">
                    <span>
                        <i class="la la-edit"></i>
                        <span>Ubah</span>
                    </span>
                    </a>';
                    $show_button_delete = '<a href="#" class="btn btn-danger m-btn btn-sm m-btn m-btn--icon" id="btn-delete-surat-masuk"
                    data-id="' . $suratmasuk->id_surat . '">
                    <span>
                        <i class="la la-trash"></i>
                        <span>Hapus</span>
                    </span>
                    </a> ';
                    $show_button_approve = '';
                }else if($check_button_action == 3 && $suratmasuk->status_surat == 0){
                    $show_button_approve = '<a href="#" class="btn btn-primary m-btn btn-sm m-btn m-btn--icon" data-toggle="modal"
                    data-id= "'. $suratmasuk->id_surat.'"
                    data-nomor_surat= "'. $suratmasuk->nomor_surat.'"
                    data-target="#modal-proses" id="btn_proses_surat">
                    <span>
                        <i class="la la-send"></i>
                        <span>Cek Surat</span>
                    </span>
                    </a> ';

                    $show_button_edit = '<a href="#" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon"
                    data-toggle="modal"
                    data-suratid= "'. $suratmasuk->id_surat.'"
                    data-target="#modal-edit" id="btn_update_surat">
                    <span>
                        <i class="la la-edit"></i>
                        <span>Ubah</span>
                    </span>
                    </a></div>';
                    $show_button_delete = '';
                }
                else{
                    $show_button_edit = '';
                    $show_button_delete = '';
                    $show_button_approve = '';
                }
                $action_edit = '<center>'. $show_button_edit.'
                                ';

                $action_detail = $show_button_delete.'<a href="#" class="btn btn-accent btn-sm m-btn  m-btn m-btn--icon"
                                data-toggle="modal"
                                data-suratiddetail= "'. $suratmasuk->id_surat.'"
                                data-target="#modal-detail" id="btn_detail_surat">
                                <span>
                                    <i class="la la-info"></i>
                                    <span>Detail</span>
                                </span>
                                </a> '.$show_button_approve.'</center>';
                $status_surat = [0 => '<span style="width:70px" class="m-badge m-badge--primary"><b>Diproses</b></span>',
                                 1 => '<span style="width:70px" class="m-badge m-badge--success"><b>Disetujui</b></span>',
                                 2 => '<span style="width:70px" class="m-badge m-badge--danger"><b>Ditolak</b></span>'
                                ];

                $data = [];
                $data[] = $count++;
                $data[] = (date("d-m-Y", strtotime($suratmasuk->tgl_surat)));
                $data[] = $suratmasuk->nama == 'PENTING' ? '<b style="color:red">'.$suratmasuk->nama.'</b>' :$suratmasuk->nama ;
                $data[] = ($suratmasuk->nomor_surat == null ? '-' : $suratmasuk->nomor_surat);
                $data[] = ($suratmasuk->perihal == null ? '-' : $suratmasuk->perihal);
                if(Auth::user()->role_id == 3 || Auth::user()->role_id == 4){
                    $data[] = ($suratmasuk->catatan_approve == null ? '-' : $suratmasuk->catatan_approve);
                }

                $data[] = $status_surat[($suratmasuk->status_surat)];

                $data[] = $action_edit.' '.$action_detail ;
                $result[] = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function update_approve(Request $request)
    {
        try {
           \DB::table('tt_suratmasuk')->where('id_surat', $request->id_surat_approve)->update([
                'status_surat' => $request->approve_status_surat,
                'catatan_approve' => $request->catatan_approve
            ]);
            // dd($check);
            return response()->json(['status' => 'success', 'result' => 'Surat Berhasil diproses'], 200);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
        }
    }

    public function AjaxDetail($id_surat)
    {
        // dd(Auth::user()->jabatan_id);
        if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3 || Auth::user()->role_id == 4){
            $suratmasuk = \DB::table('tt_suratmasuk as sm')
            ->Leftjoin('tt_disposisi as dis', 'sm.id_surat', 'dis.id_surat')
                ->Leftjoin('tm_jenis_surat as js', 'sm.id_jenis', 'js.id_jenis')
                ->Leftjoin('tm_kategori as ka', 'sm.id_kategori', 'ka.id_kategori')
                ->Leftjoin('tm_jabatan as ja', 'sm.id_jabatan', 'ja.id_jabatan')
                ->Leftjoin('tm_kode_arsip as kar', 'sm.id_kode_arsip', 'kar.id_kode_arsip')

                ->select('*')
                ->where('sm.id_surat', $id_surat)
                ->first();
        // dd($suratmasuk);
        }else{
            $suratmasuk = \DB::table('tt_suratmasuk as sm')
            ->Leftjoin('tt_disposisi as dis', 'sm.id_surat', 'dis.id_surat')
                ->Leftjoin('tm_jenis_surat as js', 'sm.id_jenis', 'js.id_jenis')
                ->Leftjoin('tm_kategori as ka', 'sm.id_kategori', 'ka.id_kategori')
                ->Leftjoin('tm_jabatan as ja', 'sm.id_jabatan', 'ja.id_jabatan')
                ->Leftjoin('tm_kode_arsip as kar', 'sm.id_kode_arsip', 'kar.id_kode_arsip')

                ->select('*')
                ->where('sm.id_surat', $id_surat)
                ->where('dis.id_usrz', Auth::user()->jabatan_id)
                ->first();
        }

        // dd($suratmasuk);
        return response()->json(['status'=> 'success', 'result'=> $suratmasuk], 200);

    }

    public function simpan(Request $request){
        // dd($request->file_upload);
        // $request->file('file_upload');
        $validator = \Validator::make($request->all(), [
            'tgl_surat_terima' => 'required',
            'tgl_surat' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }


        if($request->file_upload == null){
            $request->file_upload == null;
        }else{
            $file_pdf_jpg = $request->file('file_upload');
            $arsip = DB::table('tm_kode_arsip')->select('*')->where('id_kode_arsip', $request->id_kode_arsip)->first();

            if ($file_pdf_jpg) {
                $file = $arsip->kode_arsip.'-'.$request->nomor_surat. '.' . $file_pdf_jpg->getClientOriginalExtension();
                $filepath = public_path('FILEUPLOAD');
                if (!is_dir($filepath)) {
                    File::makeDirectory(public_path('FILEUPLOAD'));
                }
                $upload = $file_pdf_jpg->move($filepath, $file);
                if ($upload) {
                    $request->file_upload = $file;
                }
            } else {
                $request->file_upload = null;
            }
        }

        $reverse_tgl_surat_terima = date("Y-m-d", strtotime($request->tgl_surat_terima));
        $reverse_tgl_surat = date("Y-m-d", strtotime($request->tgl_surat));
        DB::table('tt_suratmasuk')->insert([
                            'tgl_surat'  => $reverse_tgl_surat,
                            'id_jenis' => $request->id_jenis,
                            'nomor_surat'           => $request->nomor_surat,
                            'perihal'               => $request->perihal,
                            'dari'                  => $request->dari,
                            'id_jabatan'            => $request->id_jabatan,
                            'kepada'                => $request->kepada,
                            'file_upload'           => $request->file_upload,
                            'id_usrz'               => Auth::user()->id,
                            'tgl_surat_terima'      => $reverse_tgl_surat_terima,
                            'id_kode_arsip'         => $request->id_kode_arsip,
                            'id_kategori'           => $request->id_kategori,
                            'catatan'               => $request->catatan,
                            'nomor_agenda'          => $request->nomor_agenda,
                            'read_surat'            => 0,
                            'created_at'            => \Carbon\Carbon::now()
                ]);
        // dd($query);
        return response()->json(['success'=>'Data berhasil ditambahkan']);
    }

    public function update(Request $request)
    {
        // dd($request->file('file_upload'));
       // dd($request->all());
       $rules = [
        'tgl_surat' => 'required',
        'tgl_surat_terima' => 'required'
    ];
    $messages = [
        'required' => 'The :attribute is required.',
        'min' => 'The :attribute is lest than 3 character.',
    ];
    //validation roles
    $validator = Validator::make($request->all(), $rules, $messages);
    if ($validator->fails()) {
        return response()->json(['status' => 'error', 'message' => $validator->errors()->all()], 406);
    }
    $query_check = \DB::table('tt_suratmasuk')->where('id_surat', $request->id_surat)->first();
    $image = $request->file('file_upload');
    // dd($image);
    if ($image) {
        if(File::exists(trim(public_path('FILEUPLOAD').'/'.$query_check->file_upload))){
                // unlink(trim(public_path('file_upload').'/'.$query_check->file_upload));
                Storage::delete(trim(public_path('FILEUPLOAD').'/'.$query_check->file_upload));
        }
        $arsip = DB::table('tm_kode_arsip')->select('*')->where('id_kode_arsip', $request->id_kode_arsip)->first();
        $employee_image = $arsip->kode_arsip.'-'.$request->nomor_surat. '.' . $image->getClientOriginalExtension();

        $filepath = public_path('FILEUPLOAD');
        if (!is_dir($filepath)) {
            \File::makeDirectory(public_path('FILEUPLOAD'));
        }
        $upload = $image->move($filepath, $employee_image);
        if ($upload) {
            $upload_db = $employee_image;
        }
    } else {
        $upload_db = $query_check->file_upload;
    }

    //  dd($request->all());
    $reverse_tgl_surat_terima = date("Y-m-d", strtotime($request->tgl_surat_terima));
    $reverse_tgl_surat = date("Y-m-d", strtotime($request->tgl_surat));
    try {
        DB::table('tt_suratmasuk')->where('id_surat', $request->id_surat)
                                  ->update([
                                        'tgl_surat'  => $reverse_tgl_surat,
                                        'id_jenis' => $request->id_jenis,
                                        'nomor_surat'           => $request->nomor_surat,
                                        'perihal'               => $request->perihal,
                                        'dari'                  => $request->dari,
                                        'id_jabatan'            => $request->id_jabatan,
                                        'kepada'                => $request->kepada,
                                        'file_upload'           => $upload_db,
                                        'id_usrz'               => Auth::user()->id,
                                        'tgl_surat_terima'      => $reverse_tgl_surat_terima,
                                        'id_kode_arsip'         => $request->id_kode_arsip,
                                        'id_kategori'           => $request->id_kategori,
                                        'catatan'               => $request->catatan,
                                        'nomor_agenda'          => $request->nomor_agenda,
                                        'updated_at'            => \Carbon\Carbon::now()
                                    ]);
        // dd($request->all());
        return response()->json(['status' => 'success', 'result' => 'Data berhasil diubah'], 200);
    } catch (\Exception $exception) {
        return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
    }
    }

    public function destroy(Request $request)
    {
        try {
           DB::table('tt_suratmasuk')->where('id_surat', '=', $request->id)->delete();
           DB::table('tt_disposisi')->where('id_surat', '=', $request->id)->delete();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 404);
        }
        return response()->json(['status' => 'success', 'result' => 'Data berhasil dihapus'], 200);
    }

    public function show_data_print($id_surat){
        $query = DB::select(DB::raw('SELECT
                                    tm_jenis_surat.nama as jenis_surat,
                                    tt_suratmasuk.id_surat,
                                    tt_suratmasuk.tgl_entry,
                                    tt_suratmasuk.tgl_surat,
                                    tt_suratmasuk.id_jenis,
                                    tt_suratmasuk.nomor_surat,
                                    tt_suratmasuk.perihal,
                                    tt_suratmasuk.dari,
                                    tm_kategori.kategori,
                                    tt_suratmasuk.kepada,
                                    tm_jabatan.nama_jabatan,
                                    tt_suratmasuk.file_upload,
                                    tt_suratmasuk.id_usrz
                                FROM
                                    tm_jenis_surat
                                    INNER JOIN tt_suratmasuk ON (tm_jenis_surat.id_jenis = tt_suratmasuk.id_jenis)
                                    INNER JOIN tm_jabatan ON (tt_suratmasuk.id_jabatan = tm_jabatan.id_jabatan)
                                    INNER JOIN tm_kategori ON (tt_suratmasuk.id_kategori = tm_kategori.id_kategori)
                                    WHERE id_surat = "'.$id_surat.'"
                                ORDER BY
                                    id_surat DESC ' ));

        // DD($query);
        $data['view'] =  $query;
        // dd( $data['view']);
        return view('suratmasuk/print_suratmasuk', $data);

    }
}
