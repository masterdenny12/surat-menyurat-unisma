<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Illuminate\Support\Arr;
use App\Helpers\notif;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller

{
    public function index(Request $request)

    {
        $notif = notif::notifikasi();
        return view('users.index', compact('notif'));
    }

    public function show_datatable(){
        try {
            $result = [];
            $count = 1;
            $query = DB::table('users')
                    ->get();
            // dd($query);
            foreach ($query as $user) {
                $action_edit = '<center>
                                <a href="#" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon"
                                data-toggle="modal"
                                data-id= "'. $user->id.'"
                                data-target="#modal-edit" id="btn_update_surat">
                                <span>
                                    <i class="la la-edit"></i>
                                    <span>Ubah</span>
                                </span>
                                </a>';

                $action_detail = '<a href="#" class="btn btn-danger m-btn btn-sm m-btn m-btn--icon" id="btn-delete"
                                data-id="' . $user->id . '">
                                <span>
                                    <i class="la la-trash"></i>
                                    <span>Hapus</span>
                                </span>
                                </a> </center>';

                $data = [];
                $data[] = $count++;
                $data[] = ($user->name);
                $data[] = ($user->username);
                $data[] = ($user->email);
                $data[] = $action_edit.' '.$action_detail ;
                $result[] = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function simpan(Request $request){

        DB::table('users')->insert([
                            'name'          => $request->name,
                            'username'      => $request->username,
                            'email'         => $request->email,
                            'jabatan_id'    => $request->jabatan_id,
                            'password'      => bcrypt($request->password),
                            'role_id'       => $request->role_id

                ]);
        // dd($query);
        return response()->json(['success'=>'Data berhasil ditambahkan']);
    }

    public function update(Request $request)
    {
    try {
        DB::table('users')->where('id', $request->id)
                                  ->update([
                                        'name'          => $request->name,
                                        'username'      => $request->username,
                                        'email'         => $request->email,
                                        'jabatan_id'    => $request->jabatan_id,
                                        'password'      => bcrypt($request->password),
                                        'role_id'       => $request->role_id
                                    ]);
        // dd($request->all());
        return response()->json(['status' => 'success', 'result' => 'Data berhasil diubah'], 200);
    } catch (\Exception $exception) {
        return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
    }

    }

    public function destroy(Request $request)
    {
        try {
           DB::table('users')->where('id', '=', $request->id)->delete();

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 404);
        }
        return response()->json(['status' => 'success', 'result' => 'Data berhasil dihapus'], 200);
    }

    public function AjaxDetail($id_user)
    {
        $users = \DB::table('users')
            ->Leftjoin('tm_jabatan', 'users.jabatan_id', 'tm_jabatan.id_jabatan')
            ->select('*')
            ->where('id', $id_user)
            ->first();
        // dd($suratmasuk);
        return response()->json(['status'=> 'success', 'result'=> $users], 200);

    }


}
