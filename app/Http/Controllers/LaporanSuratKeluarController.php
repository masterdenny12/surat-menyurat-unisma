<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Helpers\notif;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PDF;

class LaporanSuratKeluarController extends Controller
{
    public function index()
    {
        $notif = notif::notifikasi();
        $notif_surat_keluar = notif::notifikasi_surat_keluar();
        return view('laporan.suratkeluar', compact('notif', 'notif_surat_keluar'));
    }

    public function show_datatable(){
        try {
            $result = [];
            $count = 1;

            $query = DB::select('select * from tt_suratkeluar LEFT JOIN tm_jenis_surat ON tt_suratkeluar.id_jenis = tm_jenis_surat.id_jenis ');
            // dd($query);
            foreach ($query as $suratkeluar) {

                $data = [];
                $data[] = $count++;
                $data[] = (date("d-m-Y", strtotime($suratkeluar->tgl_suratkel)));
                $data[] = $suratkeluar->nama == 'PENTING' ? '<b style="color:red">'.$suratkeluar->nama.'</b>' :$suratkeluar->nama ;
                $data[] = ($suratkeluar->nomor == null ? '-' : $suratkeluar->nomor);
                $data[] = ($suratkeluar->perihal == null ? '-' : $suratkeluar->perihal);
                $data[] = ($suratkeluar->kepada == null ? '-' : $suratkeluar->kepada);

                // $data[] = $action_edit.' '.$action_hapus ;
                $result[] = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function exportExcelLaporanSuratKeluar(){

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $i = 5;
        $no=1;
        $query = DB::select('select * from tt_suratkeluar LEFT JOIN tm_jenis_surat ON tt_suratkeluar.id_jenis = tm_jenis_surat.id_jenis ');
        // dd($query);

        $sheet->setCellValue('B2', 'DATA SURAT');

        $sheet->setCellValue('A3', 'NO');
        $sheet->setCellValue('B3', 'Tanggal Surat');
        $sheet->setCellValue('C3', 'Jenis');
        $sheet->setCellValue('D3', 'Nomor Surat');
        $sheet->setCellValue('E3', 'Perihal');
        $sheet->setCellValue('F3', 'kepada');

        //filte
        $sheet->setAutoFilter('A1:AC1');
        //color cell
            //warna nomer

            //warna depart

        //freeze pane

        $sheet->freezePaneByColumnAndRow(1,'D1');
        $sheet->freezePane('D5');

        //merge cells
        $sheet->mergeCells("B2:D2");
        $sheet->mergeCells("A3:A4");
        $sheet->mergeCells("B3:B4");
        $sheet->mergeCells("C3:C4");

        //size cells
        $sheet->getColumnDimension('A')->setWidth(5);
        $sheet->getColumnDimension('B')->setWidth(17);


        $styleArray = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
             ],
             'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],

        ];
        $styleHeader = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
             ],
             'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],

        ];
        $styletitle = [

            'font' => [
                'name' => 'Century Gothic',
                'size' => 14,
                'bold' => true,
                'color' => ['argb' => '000000'],
            ],

        ];
        $styleheader = [

            'font' => [
                'size' => 11,
                'bold' => true,
                'color' => ['argb' => '000000'],
            ],

        ];
        $sheet->getStyle('A5:I50')->applyFromArray($styleArray);
        $sheet->getStyle('A3:I4')->applyFromArray($styleHeader);
        $sheet->getStyle('A2:D2')->applyFromArray($styletitle);
        $sheet->getStyle('A3:I3')->applyFromArray($styleheader);

        foreach ($query as $key => $value) {
            $tgl_suratkel = $value->tgl_suratkel;
            $reverse_tgl_suratkel = date("d-M-Y", strtotime($tgl_suratkel));
            // $tanggal_keluar = $value->tanggal_keluar;
            // $reverse_tanggal_keluar = date("d-M-Y", strtotime($tanggal_keluar));

            $sheet->setCellValue('A'.$i, $no++);
            $sheet->setCellValue('B'.$i, $reverse_tgl_suratkel);
            $sheet->setCellValue('C'.$i, $value->nama);
            $sheet->setCellValue('D'.$i, $value->nomor);
            $sheet->setCellValue('E'.$i, $value->perihal);
            $sheet->setCellValue('F'.$i, $value->kepada);

            $i++;
        }

        $writer = new Xlsx($spreadsheet);
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="file.xlsx"');
        $writer->save("php://output");
        }

        public function downloadPDF(){
            $query = DB::select('select * from tt_suratkeluar LEFT JOIN tm_jenis_surat ON tt_suratkeluar.id_jenis = tm_jenis_surat.id_jenis ');
            $pdf = PDF::loadview('laporan.view_pdf_surat_keluar',['laporan' => $query]);
            $pdf->setPaper('A4', 'portrait');
            $name_file = 'Laporan-surat-keluar '. date('d-m-y').'.pdf';
            return $pdf->download($name_file);
            // return $pdf->stream();
        }
}
