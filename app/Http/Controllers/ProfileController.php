<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Helpers\notif;

class ProfileController extends Controller
{
    public function index()
    {
        $notif = notif::notifikasi();
        $notif_surat_keluar = notif::notifikasi_surat_keluar();
        $data = $this->data_profile();
        // dd($data);
        return view('profile.index', compact('data', 'notif', 'notif_surat_keluar'));
    }

    public function data_profile(){
        $db = DB::table('users')
                ->Leftjoin('tm_jabatan', 'users.jabatan_id', 'tm_jabatan.id_jabatan')
                ->where('jabatan_id', Auth::user()->jabatan_id)
                ->first();
        $nama   = Auth::user()->name;
        $email  = Auth::user()->email;
        $jabatan = $db->nama_jabatan;
        $array = [];
        array_push($array, [
                            'nama'      => $nama,
                            'email'     => $email,
                            'jabatan'   => $jabatan
                        ]);
        return $array;
    }
    public function editPassword(Request $request)
    {

        $rules = [
            'current_pass' => 'required|min:4',
            'new_pass' => 'required|min:4',
            'confirm_pass' => 'required|same:new_pass',
        ];

        $messages = [
            'required' => 'The :attribute is required.',
            'min' => 'The :attribute is lest than 3 character.',
        ];
        //validation roles
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->all()], 406);
        }

        //get user data with id match
        $user_query = \DB::table('users')->where('id', $request->id)->first();
        if (!$user_query) {
            return \response()->json(['status' => 'error', 'message' => 'user not found'], 406);
        }

        try {
            if (\Hash::check($request->current_pass, $user_query->password)) {
                \DB::table('users')
                    ->where('id', $request->id)
                    ->update([
                        'password' => \Hash::make($request->new_pass)
                    ]);
            }else{
                return \response()->json(['status' => 'error', 'message' => 'current password doesn\'t match '], 406);
            }

        } catch (\Exception $exception) {
            return \response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
        }
        return \response()->json(['status' => 'success', 'result' => 'Reset password was successfully'], 200);

    }
}
