<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komponen_kompetensi_model extends Model
{
    protected $table = 'm_komponen_kompetensi_dosen';
    protected $fillable = [
        'sub_kategori_dosen_id', 'komponen_kompetensi',
        'deskripsi_skor1', 'deskripsi_skor2', 'deskripsi_skor3',
        'deskripsi_skor4', 'deskripsi_skor5',
        'bukti_pendukung'

    ];
}
