<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class cust_crud {
    public static function create($table, array $field) {
        $query = DB::table($table)
                ->insert($field);
        return $query;
    }
}
