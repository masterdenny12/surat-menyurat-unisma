<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class notif {
    public static function notifikasi() {
        if(Auth::user()->role_id ==2){
            $notif = DB::table('tt_suratmasuk')
            ->where('id_jabatan', Auth::user()->jabatan_id)
            ->where('read_surat', 0)
            ->where('status_surat', 1)
            ->count();
        }else{
            $notif = '';
        }
        return $notif;
    }

    public static function notifikasi_surat_keluar() {
        if(Auth::user()->role_id ==2){
            $notif = DB::table('tc_suratkeluar')
            ->where('id_jabatan', Auth::user()->jabatan_id)
            ->where('read_surat', 0)
            ->count();
        }else{
            $notif = '';
        }
        return $notif;
    }
}
