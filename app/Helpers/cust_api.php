<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class cust_api {
    public static function get_data_api($url, $data=null) {
        if(!empty($data)){
            unset($data['ci_csrf_token']);
            $status = 'POST';
            $head 	= null;
        }else{
            $status= 'GET';
            $head = 'Content-Type: application/json';
        }
        // die(pre([$status, $data]));
         //server / develop
        // die(pre($url));
        // $url = 'http://128.199.165.95/product/pmi_api_dev/'.$urlnya;//local
        // $url = 'http://192.168.1.5:31/'.$urlnya;//local
        $header = [
            'SISMIOP-API-KEY: SISMIOP-VALUE',
            $head
            //'Content-Type : application/x-www-form-urlencoded'
        ];
        $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL =>$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $status,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $header,

        ));
        $json_response = curl_exec($curl);
        curl_close($curl);
        // die(pre([$data , $status,$json_response]));
        // die(pre($url));
        $response = json_decode($json_response,true);
        return $response;
    }
}
