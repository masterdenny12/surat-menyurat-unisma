<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use DB;

class ExportEmployeeActive implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $query = DB::table('karyawan')
                ->select('*')
                ->where('is_active', '=', 1)
                ->get();
        return $query;
    }
}
