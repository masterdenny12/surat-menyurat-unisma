<?php

namespace App\Karyawan;

use Illuminate\Database\Eloquent\Model;

class Bagian extends Model
{
    protected $connection ='mysql';
    protected $table = 'bagian';
   
   public function teknikalkompetensi_target(){
       return $this->hasMany('App\TeknikalKompetensi_target', 'bagian_id', 'id');
   }
   public function softkompetensi_target(){
       return $this->hasMany('App\SoftKompetensi_target', 'bagian_id');
   }
   public function karyawan(){
       return $this->hasMany(Karyawan::class, 'bagian_id');
   }
   public function department(){
       return $this->belongsTo('App\Karyawan\Department', 'department_id', 'id');
   }
}
