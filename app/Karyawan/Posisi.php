<?php

namespace App\Karyawan;

use Illuminate\Database\Eloquent\Model;

class Posisi extends Model
{
    protected $connection = 'mysql';
    protected $table = 'posisi';

    public function karyawan()
    {
        return $this->hasMany('App\Karyawan\Karyawan', 'posisi_id', 'id');
    }

    public function teknikalkompetensi_target()
    {
        return $this->hasMany('App\TeknikalKompetensi_target', 'posisi_id', 'id');
    }
    public function softkompetensi_target()
    {
        return $this->hasMany('App\SoftKompetensi_target', 'posisi_id', 'id');
    }
}
