<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use App\penilaianDosenModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportPenilaianDosen implements ToModel, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        return new penilaianDosenModel([
                        'pegawai_id'    => 11111111,
                        'bln_penilaian_id'    => $row['bln_penilaian_id'],
                        'kategori_penilaian_dosen_id'    => $row['kategori_penilaian_dosen_id'],
                        'sub_kategori_penilaian_dosen_id'    => $row['sub_kategori_penilaian_dosen_id'],
                        'skor_capaian'      => $row['skor_capaian'],

        ]);


    }
}
