<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class penilaianDosenModel extends Model
{
    protected $table = 'p_penilaian_pegawai_dosen';
    protected $fillable = [
        'pegawai_id', 'bln_penilaian_id',
        'kategori_penilaian_dosen_id', 'sub_kategori_penilaian_dosen_id', 'skor_capaian',

    ];
}
