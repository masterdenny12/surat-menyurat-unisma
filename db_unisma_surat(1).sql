-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 18 Okt 2020 pada 16.45
-- Versi server: 5.7.26
-- Versi PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_unisma_surat`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tc_suratkeluar`
--

DROP TABLE IF EXISTS `tc_suratkeluar`;
CREATE TABLE IF NOT EXISTS `tc_suratkeluar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_suratkel` int(11) DEFAULT '0',
  `id_jabatan` int(11) NOT NULL DEFAULT '0',
  `read_surat` int(11) NOT NULL DEFAULT '0',
  `idd` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=151 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tc_suratkeluar`
--

INSERT INTO `tc_suratkeluar` (`id`, `id_suratkel`, `id_jabatan`, `read_surat`, `idd`, `created_at`, `updated_at`) VALUES
(150, 1, 3, 0, 0, '2020-10-18 16:41:52', '2020-10-18 16:41:52'),
(149, 1, 2, 0, 0, '2020-10-18 16:41:52', '2020-10-18 16:41:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_format_surat_keluar`
--

DROP TABLE IF EXISTS `tm_format_surat_keluar`;
CREATE TABLE IF NOT EXISTS `tm_format_surat_keluar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_surat` varchar(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `file_surat` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_format_surat_keluar`
--

INSERT INTO `tm_format_surat_keluar` (`id`, `jenis_surat`, `keterangan`, `file_surat`, `created_at`, `updated_at`) VALUES
(6, 'KEPUTUSAN REKTOR', 'keputusan rektor', 'Format Surat - KEPUTUSAN REKTOR.docx', '2020-10-18 10:44:13', '2020-10-18 10:53:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_jabatan`
--

DROP TABLE IF EXISTS `tm_jabatan`;
CREATE TABLE IF NOT EXISTS `tm_jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `nama_jabatan` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`id_jabatan`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_jabatan`
--

INSERT INTO `tm_jabatan` (`id_jabatan`, `nama_jabatan`) VALUES
(1, 'REKTOR'),
(2, 'WAKIL REKTOR 1 BIDANG AKADEMIK DAN KERJASAMA'),
(3, 'WAKIL REKTOR 2 BIDANG ADMINISTRASI UMUM, PERSONALIA DAN KEUANGAN'),
(4, 'WAKIL REKTOR 3 BIDANG KEMAHASISWAAN, ALUMNI DAN KEAGAMAAN'),
(5, 'WAKIL REKTOR 4 BIDANG KELEMBAGAAN, PUBLIKASI, DAN TEKNOLOGI INFORMASI'),
(6, 'DIREKTUR PROGRAM PASCASARJANA'),
(7, 'DEKAN FAKULTAS AGAMA ISLAM'),
(8, 'DEKAN FAKULTAS HUKUM'),
(9, 'DEKAN FAKULTAS PERTANIAN'),
(10, 'DEKAN FAKULTAS PETERNAKAN'),
(11, 'DEKAN FAKULTAS TEKNIK'),
(12, 'DEKAN FAKULTAS MIPA'),
(13, 'DEKAN FAKULTAS KEGURUAN DAN ILMU PENDIDIKAN'),
(14, 'DEKAN FAKULTAS EKONOMI DAN BISNIS'),
(15, 'DEKAN FAKULTAS ILMU ADMINISTRASI'),
(16, 'DEKAN FAKULTAS KEDOKTERAN'),
(17, 'WAKIL DIREKTUR 1 PROGRAM PASCASARJANA'),
(18, 'WAKIL DIREKTUR 2 PROGRAM PASCASARJANA'),
(19, 'WAKIL DEKAN 1 FAKULTAS AGAMA ISLAM'),
(20, 'WAKIL DEKAN 2 FAKULTAS AGAMA ISLAM'),
(21, 'WAKIL DEKAN 3 FAKULTAS AGAMA ISLAM'),
(22, 'WAKIL DEKAN 1 FAKULTAS HUKUM'),
(23, 'WAKIL DEKAN 2 FAKULTAS HUKUM'),
(24, 'PJS. WAKIL DEKAN 3 FAKULTAS HUKUM'),
(25, 'WAKIL DEKAN 1 FAKULTAS PERTANIAN'),
(26, 'WAKIL DEKAN 2 FAKULTAS PERTANIAN'),
(27, 'WAKIL DEKAN 3 FAKULTAS PERTANIAN'),
(28, 'WAKIL DEKAN 1 PETERNAKAN'),
(29, 'WAKIL DEKAN 2 PETERNAKAN'),
(30, 'WAKIL DEKAN 1 FAKULTAS TEKNIK'),
(31, 'WAKIL DEKAN 2 FAKULTAS TEKNIK'),
(32, 'WAKIL DEKAN 3 FAKULTAS TEKNIK'),
(33, 'WAKIL DEKAN 1 FAKULTAS MIPA'),
(34, 'PJS. WAKIL DEKAN 2 FAKULTAS MIPA'),
(35, 'WAKIL DEKAN 1 FAKULTAS KIP'),
(36, 'WAKIL DEKAN 2 FAKULTAS KIP'),
(37, 'WAKIL DEKAN 3 FAKULTAS KIP'),
(38, 'WAKIL DEKAN 1 FAKULTAS EKONOMI DAN BISNIS'),
(39, 'WAKIL DEKAN 2 FAKULTAS EKONOMI DAN BISNIS'),
(40, 'WAKIL DEKAN 3 FAKULTAS EKONOMIDAN BISNIS'),
(41, 'WAKIL DEKAN 1 FAKULTAS ILMU ADMINISTRASI'),
(42, 'PJS. WAKIL DEKAN 2 FAKULTAS ILMU ADMINISTRASI'),
(43, 'WAKIL DEKAN 3 FAKULTAS ILMU ADMINISTRASI'),
(44, 'WAKIL DEKAN 1 FAKULTAS KEDOKTERAN'),
(45, 'WAKIL DEKAN 2 FAKULTAS KEDOKTERAN'),
(46, 'WAKIL DEKAN 3 FAKULTAS KEDOKTERAN'),
(47, 'KEPALA BIRO BIDANG ADMINISTRASI AKADEMIK DAN KERJASAMA'),
(48, 'KABAG. AKADEMIK'),
(49, 'KABAG. REGISTRASI DAN STATISTIK'),
(50, 'KABAG. KERJASAMA'),
(51, 'KEPALA BIRO BIDANG ADMINISTRASI UMUM, PERSONALIA DAN KEUANGAN'),
(52, 'KEPALA BAGIAN UMUM DAN KERUMAHTANGGAAN'),
(53, 'KEPALA BAGIAN PERLENGKAPAN DAN ASSET'),
(54, 'KEPALA BAGIAN PERSONALIA'),
(55, 'KEPALA BAGIAN KEUANGAN'),
(56, 'KEPALA BIRO BIDANG KEMAHASISWAAN, ALUMNI, DAN KEAGAMAAN'),
(57, 'KEPALA BAGIAN KEMAHASISWAAN'),
(58, 'KEPALA BAGIAN ALUMNI'),
(59, 'KEPALA BAGIAN KEAGAMAAN'),
(60, 'KEPALA BIRO ADMINISTRASI KELEMBAGAAN, PUBLIKASI DAN TEKNOLOGI  INFORMASI'),
(61, 'KEPALA BAGIAN KELEMBAGAAN'),
(62, 'KEPALA BAGIAN PUBLIKASI DAN PMB'),
(63, 'KEPALA BAGIAN HUMAS'),
(64, 'KETUA BADAN PENJAMINAN MUTU'),
(65, 'KETUA LEMBAGA PENELITIAN DAN PENGABDIAN MASYARAKAT (LPPM)'),
(66, 'KETUA LEMBAGA PENGEMBANGAN PEMBELAJARAN DAN RELEVANSI PENDIDIKAN (LP2RP)'),
(67, 'KETUA LEMBAGA PENGKAJIAN ISLAM DAN KEASWAJAAN (LPIK)'),
(68, 'KEPALA PUSAT PENGEMBANGAN KEWIRAUSAHAAN DAN INKUBATOR BISNIS (PPKIB) MERANG'),
(69, 'KEPALA KANTOR URUSAN INTERNASIONAL (KUI)'),
(70, 'KEPALA PUSAT PENGEMBANGAN BAHASA ASING MERANGKAP BIDANG PENGEMBANGAN BAHASA'),
(71, 'KEPALA LABORATORIUM PUSAT DAN HALAL CENTER (PHC)'),
(72, 'KETUA SENTRA HAKI DAN INOVASI'),
(73, 'KEPALA PERPUSTAKAAN PUSAT'),
(74, 'DIREKTUR PONDOK PESANTREN DAN MASJID AINUL YAQIN'),
(75, 'MANAJER RUSUNAWA'),
(76, 'KEPALA SATUAN PENGAWAS INTERNAL (SPI)'),
(77, 'KEPALA P3TIK'),
(78, 'KETUA TASKFORCE'),
(79, 'KEPALA UPT'),
(80, 'PJS. KPS PENDIDIKAN AGAMA ISLAM'),
(81, 'PJS. KPS AHWAL AS-SYAKHSYIAH'),
(82, 'PJS. KPS PEND. GURU MAD. IBTIDAIYAH'),
(83, 'PJS. KPS PEND. GURU RAUDLATUL ATHFAL'),
(84, 'KPS BAHASA ARAB'),
(85, 'KPS ILMU HUKUM'),
(86, 'KPS AGROTEKNOLOGI'),
(87, 'PJS. KPS AGRIBISNIS'),
(88, 'KPS PETERNAKAN'),
(89, 'KPS TEKNIK SIPIL'),
(90, 'PJS. KPS TEKNIK MESIN'),
(91, 'KPS TEKNIK ELEKTRO'),
(92, 'PJS. KPS BIOLOGI'),
(93, 'PJS. KPS PEND. BHS INDONESIA'),
(94, 'KPS PEND. MATEMATIKA'),
(95, 'KPS PEND. BAHASA INGGRIS'),
(96, 'KPS PROFESI PPG'),
(97, 'KPS MANAJEMEN '),
(98, 'KPS AKUNTANSI '),
(99, 'PJS. KPS PERBANKAN SYARIAH'),
(100, 'PJS. KPS LLMU ADM. PUBLIK '),
(101, 'PJS. KPS LLMU ADM. BISNIS '),
(102, 'KPS PENDIDIKAN DOKTER'),
(103, 'KPS PROFESI DOKTER'),
(104, 'KPS FARMASI'),
(105, 'KPS MAGISTER PEND. LSLAM DAN HUKUM LSLAM'),
(106, 'KPS MAGISTER ILMU HUKUM'),
(107, 'KPS MAGISTER KENOTARIATAN'),
(108, 'KPS MAGISTER PETERNAKAN'),
(109, 'KPS MAGISTER PENDIDIKAN B. LNDONESIA'),
(110, 'KPS MAGISTER PENDIDIKAN B. LNGGRIS'),
(111, 'KPS MAGISTER MANAJEMEN'),
(112, 'KPS MAGISTER LLMU ADMINISTRASI'),
(113, 'DIREKTUR PROGRAM PASCASARJANA'),
(114, 'WAKIL DIREKTUR  1 PROGRAM PASCASARJANA'),
(115, 'WAKIL DIREKTUR  2 PROGRAM PASCASARJANA'),
(116, 'KPS MAGISTER PEND. LSLAM DAN HUKUM LSLAM'),
(117, 'KPS MAGISTER ILMU HUKUM'),
(118, 'KPS MAGISTER KENOTARIATAN'),
(119, 'KPS MAGISTER PETERNAKAN'),
(120, 'KPS MAGISTER PENDIDIKAN B. LNDONESIA'),
(121, 'KPS MAGISTER PENDIDIKAN B. LNGGRIS'),
(122, 'KPS MAGISTER MANAJEMEN'),
(123, 'KPS MAGISTER LLMU ADMINISTRASI'),
(124, 'KPS PROGRAM DOKTOR PAI '),
(125, 'DEKAN FAKULTAS AGAMA ISLAM'),
(126, 'WAKIL DEKAN 1 FAKULTAS AGAMA ISLAM'),
(127, 'WAKIL DEKAN 2 FAKULTAS AGAMA ISLAM'),
(128, 'WAKIL DEKAN 3 FAKULTAS AGAMA ISLAM'),
(129, 'PJS. KPS PENDIDIKAN AGAMA ISLAM'),
(130, 'PJS. KPS AHWAL AS-SYAKHSYIAH'),
(131, 'PJS. KPS PEND. GURU MAD. IBTIDAIYAH'),
(132, 'PJS. KPS PEND. GURU RAUDLATUL ATHFAL'),
(133, 'DEKAN FAKULTAS HUKUM'),
(134, 'WAKIL DEKAN 1 FAKULTAS HUKUM'),
(135, 'WAKIL DEKAN 2 FAKULTAS HUKUM'),
(136, 'PJS. WAKIL DEKAN 3 FAKULTAS HUKUM'),
(137, 'KPS ILMU HUKUM'),
(138, 'DEKAN FAKULTAS PERTANIAN'),
(139, 'WAKIL DEKAN 1 FAKULTAS PERTANIAN'),
(140, 'WAKIL DEKAN 2 FAKULTAS PERTANIAN'),
(141, 'WAKIL DEKAN 3 FAKULTAS PERTANIAN'),
(142, 'KPS AGROTEKNOLOGI'),
(143, 'PJS. KPS AGRIBISNIS'),
(144, 'DEKAN FAKULTAS PETERNAKAN'),
(145, 'WAKIL DEKAN 1 PETERNAKAN'),
(146, 'WAKIL DEKAN 2 PETERNAKAN'),
(147, 'KPS PETERNAKAN'),
(148, 'DEKAN FAKULTAS TEKNIK'),
(149, 'WAKIL DEKAN 1 FAKULTAS TEKNIK'),
(150, 'WAKIL DEKAN 2 FAKULTAS TEKNIK'),
(151, 'WAKIL DEKAN 3 FAKULTAS TEKNIK'),
(152, 'KPS TEKNIK SIPIL'),
(153, 'PJS. KPS TEKNIK MESIN'),
(154, 'KPS TEKNIK ELEKTRO'),
(155, 'DEKAN FAKULTAS MIPA'),
(156, 'WAKIL DEKAN 1 FAKULTAS MIPA'),
(157, 'PJS. WAKIL DEKAN 2 FAKULTAS MIPA'),
(158, 'PJS. KPS BIOLOGI'),
(159, 'DEKAN FAKULTAS KEGURUAN DAN ILMU PENDIDIKAN'),
(160, 'WAKIL DEKAN 1 FAKULTAS KIP'),
(161, 'WAKIL DEKAN 2 FAKULTAS KIP'),
(162, 'WAKIL DEKAN 3 FAKULTAS KIP'),
(163, 'PJS. KPS PEND. BHS INDONESIA'),
(164, 'KPS PEND. MATEMATIKA'),
(165, 'KPS PEND. BAHASA INGGRIS'),
(166, 'KPS PROFESI PPG'),
(167, 'DEKAN FAKULTAS EKONOMI DAN BISNIS'),
(168, 'WAKIL DEKAN 1 FAKULTAS EKONOMI DAN BISNIS'),
(169, 'WAKIL DEKAN 2 FAKULTAS EKONOMI DAN BISNIS'),
(170, 'WAKIL DEKAN 3 FAKULTAS EKONOMI DAN BISNIS'),
(171, 'KPS MANAJEMEN '),
(172, 'KPS AKUNTANSI '),
(173, 'PJS. KPS PERBANKAN SYARIAH'),
(174, 'DEKAN FAKULTAS ILMU ADMINISTRASI'),
(175, 'WAKIL DEKAN 1 FAKULTAS ILMU ADMINISTRASI'),
(176, 'PJS. WAKIL DEKAN 2 FAKULTAS ILMU ADMINISTRASI'),
(177, 'WAKIL DEKAN 3 FAKULTAS ILMU ADMINISTRASI'),
(178, 'PJS. KPS LLMU ADM. PUBLIK '),
(179, 'PJS. KPS LLMU ADM. BISNIS '),
(180, 'DEKAN FAKULTAS KEDOKTERAN'),
(181, 'WAKIL DEKAN 1 FAKULTAS KEDOKTERAN'),
(182, 'WAKIL DEKAN 2 FAKULTAS KEDOKTERAN'),
(183, 'WAKIL DEKAN 3 FAKULTAS KEDOKTERAN'),
(184, 'KPS PENDIDIKAN DOKTER'),
(185, 'KPS PROFESI DOKTER'),
(186, 'KPS FARMASI'),
(187, 'ADMIN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_jenis_surat`
--

DROP TABLE IF EXISTS `tm_jenis_surat`;
CREATE TABLE IF NOT EXISTS `tm_jenis_surat` (
  `id_jenis` smallint(6) NOT NULL AUTO_INCREMENT,
  `nama` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`id_jenis`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_jenis_surat`
--

INSERT INTO `tm_jenis_surat` (`id_jenis`, `nama`) VALUES
(1, 'BIASA'),
(2, 'PENTING'),
(3, 'RAHASIA');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_kategori`
--

DROP TABLE IF EXISTS `tm_kategori`;
CREATE TABLE IF NOT EXISTS `tm_kategori` (
  `id_kategori` smallint(6) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`id_kategori`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_kategori`
--

INSERT INTO `tm_kategori` (`id_kategori`, `kategori`) VALUES
(1, 'Elektronik'),
(2, 'Non Elektronik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_kode_arsip`
--

DROP TABLE IF EXISTS `tm_kode_arsip`;
CREATE TABLE IF NOT EXISTS `tm_kode_arsip` (
  `id_kode_arsip` varchar(20) DEFAULT NULL,
  `kode_arsip` varchar(50) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_kode_arsip`
--

INSERT INTO `tm_kode_arsip` (`id_kode_arsip`, `kode_arsip`, `keterangan`) VALUES
('5', 'B2', 'Perencanaan Penelitian'),
('1', 'A1', 'Penerimaan Mahasiswa Baru'),
('2', 'A2', 'Registrasi Mahasiswa'),
('3', 'A3', 'Perkuliahan'),
('4', 'B1', 'Penawaran Penelitian');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tt_disposisi`
--

DROP TABLE IF EXISTS `tt_disposisi`;
CREATE TABLE IF NOT EXISTS `tt_disposisi` (
  `id_disposisi` int(11) NOT NULL AUTO_INCREMENT,
  `id_surat` int(11) DEFAULT NULL,
  `tgl_disposisi` date DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `isi_disposisi` varchar(150) DEFAULT NULL,
  `id_usrz` int(11) DEFAULT NULL,
  `opsi` varchar(255) DEFAULT NULL,
  `jam_disposisi` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_disposisi`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tt_disposisi`
--

INSERT INTO `tt_disposisi` (`id_disposisi`, `id_surat`, `tgl_disposisi`, `id_jabatan`, `isi_disposisi`, `id_usrz`, `opsi`, `jam_disposisi`, `created_at`, `updated_at`) VALUES
(31, 9, '2020-10-16', 8, 'aaaaa', 3, 'edarkan ke unit-unit terkait', '23:19:35', '2020-10-16 16:19:35', '2020-10-16 16:19:35'),
(30, 9, '2020-10-16', 4, 'disposisi ke rektor 1 dan 2', 2, 'Mohon Pertimbangan,Mohon Pendapat,Mohon Keputusan,Mohon Petunjuk', '22:28:35', '2020-10-16 15:28:35', '2020-10-16 15:28:35'),
(29, 9, '2020-10-16', 3, 'disposisi ke rektor 1 dan 2', 2, 'Mohon Pertimbangan,Mohon Pendapat,Mohon Keputusan,Mohon Petunjuk', '22:28:35', '2020-10-16 15:28:35', '2020-10-16 15:28:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tt_suratkeluar`
--

DROP TABLE IF EXISTS `tt_suratkeluar`;
CREATE TABLE IF NOT EXISTS `tt_suratkeluar` (
  `id_suratkel` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_entry` date DEFAULT NULL,
  `tgl_suratkel` date DEFAULT NULL,
  `id_jenis` smallint(6) DEFAULT NULL,
  `nomor` varchar(50) DEFAULT NULL,
  `perihal` varchar(100) DEFAULT NULL,
  `kepada` varchar(100) DEFAULT NULL,
  `tembusan` varchar(255) DEFAULT NULL,
  `file_upload` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_suratkel`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tt_suratkeluar`
--

INSERT INTO `tt_suratkeluar` (`id_suratkel`, `tgl_entry`, `tgl_suratkel`, `id_jenis`, `nomor`, `perihal`, `kepada`, `tembusan`, `file_upload`, `user_id`, `created_at`, `updated_at`) VALUES
(1, NULL, '2020-09-29', 2, 'N09iiis988', 'PERCOBAAN SURAT TEST', 'KEPADA', '2,3', 'Surat Keluar - N09iiis988.docx', 1, '2020-10-18 16:41:51', '2020-10-18 16:41:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tt_suratmasuk`
--

DROP TABLE IF EXISTS `tt_suratmasuk`;
CREATE TABLE IF NOT EXISTS `tt_suratmasuk` (
  `id_surat` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_entry` date DEFAULT NULL,
  `tgl_surat` date DEFAULT NULL,
  `id_jenis` smallint(6) DEFAULT NULL,
  `nomor_surat` varchar(50) DEFAULT NULL,
  `perihal` varchar(100) DEFAULT NULL,
  `dari` varchar(100) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `kepada` varchar(100) DEFAULT NULL,
  `file_upload` varchar(255) DEFAULT NULL,
  `id_usrz` int(11) DEFAULT NULL,
  `id_jabatan_disposisi` int(11) DEFAULT NULL,
  `tgl_surat_terima` date DEFAULT NULL,
  `id_kode_arsip` varchar(20) DEFAULT NULL,
  `id_kategori` smallint(6) DEFAULT NULL,
  `catatan` varchar(150) DEFAULT NULL,
  `nomor_agenda` varchar(50) DEFAULT NULL,
  `read_surat` int(11) DEFAULT '0',
  `status_surat` int(11) DEFAULT '0',
  `catatan_approve` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_surat`) USING BTREE,
  KEY `tt_suratmasuk_fk1` (`id_kategori`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tt_suratmasuk`
--

INSERT INTO `tt_suratmasuk` (`id_surat`, `tgl_entry`, `tgl_surat`, `id_jenis`, `nomor_surat`, `perihal`, `dari`, `id_jabatan`, `kepada`, `file_upload`, `id_usrz`, `id_jabatan_disposisi`, `tgl_surat_terima`, `id_kode_arsip`, `id_kategori`, `catatan`, `nomor_agenda`, `read_surat`, `status_surat`, `catatan_approve`, `created_at`, `updated_at`) VALUES
(9, NULL, '2020-09-30', 2, '12345', 'perihal', 'dari', 2, NULL, 'B1-12345.jpg', 4, NULL, '2020-10-01', '4', 1, 'catat', '12345', 1, 1, NULL, '2020-10-16 14:37:15', '2020-10-16 14:52:40'),
(10, NULL, '2020-10-02', 3, '12', '1', '1', 2, NULL, 'B1-12.jpeg', 5, NULL, '2020-10-21', '4', 2, '1', '23', 0, 2, 'tidak penting suratnya', '2020-10-17 10:56:04', '2020-10-17 10:56:04'),
(11, NULL, '2020-10-13', 3, '1232', 'perihal', 'dari', 3, NULL, 'A3-1232.pdf', 5, NULL, '2020-10-20', '3', 2, 'catat', '2323', 0, 2, 'ok', '2020-10-17 11:10:02', '2020-10-17 11:10:02'),
(12, NULL, '2019-03-06', 2, '555', '55', '55', 2, NULL, 'B1-555.jpg', 5, NULL, '2020-10-13', '4', 1, '55', '555', 0, 0, NULL, '2020-10-17 12:06:10', '2020-10-17 12:06:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan_id` varchar(255) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `foto`, `jabatan_id`, `email_verified_at`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN BAAK', 'admin', 'admin@gmail.com', '', '187', NULL, '$2y$10$Q7mBH5Ob8AD.gTUjRMgVieuoUEQxzy405L3DLs5kVg4fcCSUB3m2m', 1, NULL, NULL, '2020-07-19 07:18:17'),
(2, 'wakil Rektor 1', 'wr1', 'wr1@gmail.com', '', '2', NULL, '$2y$10$SdKO5dNkqsCLZe1tlNM1TO/ccfnwpiNwu6KtD/MAotSLPqrhNd8ma', 2, NULL, '2020-06-30 19:39:29', '2020-06-30 19:39:29'),
(4, 'anik', 'anik', 'anik@gmail.com', '', '', NULL, '$2y$10$FNUsAsED6COGxhheQs0vX.ENwFlQwHIuWtLZi/T.iBPpHiN.j3DwS', 3, NULL, '2020-06-30 22:02:27', '2020-06-30 22:02:27'),
(5, 'user', 'user', 'test@gmail.com', '', '', NULL, '$2y$10$QjQ13MpkApXPHWpPw8GEouhQI./B32Hij1AddgQwGNYnwP.vRSPdu', 4, NULL, '2020-07-28 05:28:11', '2020-07-28 05:28:11'),
(3, 'wakil Rektor 2', 'wr2', 'wr2@gmail.com', '2', '3', NULL, '$2y$10$vYd0jyLMuf1rzo.XVnTXy.tZB0BdljH68r8mQz5V/DKtVdxgJ4Z62', 2, NULL, '2020-06-30 19:39:29', '2020-06-30 19:39:29'),
(12, 'wakil rektor 4', 'wr4', 'wr4@gmail.com', '', '5', NULL, '$2y$10$oxb7A24a41/yot6m5nJji.D.8AcA9WW.159Pvo2d.KCK8u2Wq9eGK', 2, NULL, NULL, NULL),
(11, 'wakil rektor 3', 'wr3', 'wr3@gmail.com', '', '4', NULL, '$2y$10$1EauK1nwhWNHRnkf0ddMD.MKVLZKIdd42BOWqqYRRBWWYk3u4mLuC', 2, NULL, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
