<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LoginController@index')->name('login')->middleware('access');
Route::post('post-login', 'LoginController@postLogin');
Route::match(['post', 'get'],'reg', 'LoginController@postRegister');

// Route::group(['middleware' => ['auth', 'loguser']], function(){
Route::group(['middleware' => ['auth', 'loguser']], function(){
Route::get('dashboard', 'DashboardController@index');
Route::get('graph_surat_masuk/{tahun}', 'DashboardController@graph_surat_masuk');
Route::get('graph_surat_keluar/{tahun}', 'DashboardController@graph_surat_keluar');
//data pegawai

//surat masuk
Route::get('suratmasuk', 'SuratMasukController@index');
Route::get('show_datatable', 'SuratMasukController@show_datatable');
Route::get('print_surat_masuk/{id_surat}', 'SuratMasukController@show_data_print');
Route::match(['get', 'post'], 'simpan_surat_masuk', 'SuratMasukController@simpan');
Route::match(['get', 'post'],'update_surat_masuk', 'SuratMasukController@update');
Route::match(['get', 'post'], 'destroy_surat_masuk', 'SuratMasukController@destroy');
Route::get('get_surat/{id_surat}', 'SuratMasukController@AjaxDetail');
Route::match(['get', 'post'], 'update_approve', 'SuratMasukController@update_approve');

//surat keluar
Route::get('suratkeluar', 'SuratKeluarController@index');
Route::get('show_datatable_surat_keluar', 'SuratKeluarController@show_datatable');
Route::match(['get', 'post'], 'simpan_surat_keluar', 'SuratKeluarController@simpan');
Route::match(['get', 'post'],'update_surat_keluar', 'SuratKeluarController@update');
Route::match(['get', 'post'], 'destroy_surat_keluar', 'SuratKeluarController@destroy');
Route::get('check_format_surat/{id}', 'SuratKeluarController@check_format_surat');
Route::get('get_surat_keluar/{id_surat}', 'SuratKeluarController@AjaxDetail');
//format surat keluar
Route::match(['get', 'post'], 'show_datatable_format_surat', 'SuratKeluarController@show_datatable_format_surat');
Route::match(['get', 'post'], 'simpan_format_surat', 'SuratKeluarController@simpanFormatSurat');
Route::get('get_format_surat/{id_surat}', 'SuratKeluarController@AjaxDetailFormatSurat');
Route::match(['get', 'post'],'update_format_surat', 'SuratKeluarController@update_format_surat');
Route::match(['get', 'post'], 'destroy_format_surat', 'SuratKeluarController@destroy_format_surat');
//users
Route::get('users', 'UserController@index');
Route::get('show_datatable_users', 'UserController@show_datatable');
Route::match(['get', 'post'], 'simpan_users', 'UserController@simpan');
Route::match(['get', 'post'],'update_users', 'UserController@update');
Route::match(['get', 'post'], 'destroy_users', 'UserController@destroy');
Route::get('get_user/{id_user}', 'UserController@AjaxDetail');

//select
Route::get('/cari/jenis_surat', 'SelectController@loadjenissurat');
Route::get('/cari/kategori_surat', 'SelectController@loadkategorisurat');
Route::get('/cari/jabatan', 'SelectController@loadjabatan');
Route::get('/cari/kode_arsip', 'SelectController@loadkodearsip');
Route::get('/cari/jabatanwithauth', 'SelectController@loadjabatanwithauth');
Route::get('/cari/formatsurat', 'SelectController@loadformatsurat');
//laporan
Route::get('laporan-surat-masuk', 'LaporanSuratMasukController@index');
Route::get('show_datatable_laporan_surat_masuk', 'LaporanSuratMasukController@show_datatable');
Route::get('export-excel-laporan-surat-masuk', 'LaporanSuratMasukController@exportExcelLaporanSuratMasuk');
Route::get('export-pdf-laporan-surat-masuk', 'LaporanSuratMasukController@downloadPDF');
Route::get('laporan-surat-keluar', 'LaporanSuratKeluarController@index');
Route::get('show_datatable_laporan_surat_keluar', 'LaporanSuratKeluarController@show_datatable');
Route::get('export-excel-laporan-surat-keluar', 'LaporanSuratKeluarController@exportExcelLaporanSuratKeluar');
Route::get('export-pdf-laporan-surat-keluar', 'LaporanSuratKeluarController@downloadPDF');
//disposisi

Route::get('disposisi', 'DisposisiSuratController@index');
Route::get('show_datatable_disposisi', 'DisposisiSuratController@show_datatable');
Route::get('view_disposisi/{id_surat}', 'DisposisiSuratController@view_disposisi');
Route::post('kirim_disposisi', 'DisposisiSuratController@simpan');
//monitoring
Route::get('monitoring', 'MonitoringController@index');
Route::get('show_datatable_monitoring', 'MonitoringController@show_datatable');
Route::get('view_monitoring/{id_surat}', 'MonitoringController@view_monitoring');
Route::get('getSuratMonitoring/{id_surat}/{id_jabatan}/{id_usrz}', 'MonitoringController@AjaxDetailMonitoring');
//profile
Route::get('profile', 'ProfileController@index');
Route::post('editPassword', 'ProfileController@editPassword');
//template word
Route::get('template_word', 'TemplateWordController@index');
//logout
Route::get('logout', 'LoginController@logout');
});

// });
