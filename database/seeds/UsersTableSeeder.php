<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Mr. Junk',
            'username' => '111111',
            'email' => 'junk@gmail.com',
            'password' => bcrypt('123456'),
            'role_id' => 1
     ]);

     App\User::create([
            'name' => 'Mr. Jenkin',
            'username' => '222222',
            'email' => 'jenkin@gmail.com',
            'password' => bcrypt('123456'),
            'role_id' => 2
     ]);

     App\User::create([
            'name' => 'Miss. Puff',
            'username' => '333333',
            'email' => 'puff@gmail.com',
            'password' => bcrypt('123456'),
            'role_id' => 3
     ]);
    }
}
