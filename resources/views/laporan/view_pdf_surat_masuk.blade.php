<html>
<head>
    <style>
        .rwd-table {
            margin: auto;
            min-width: 300px;
            max-width: 100%;
            border-collapse: collapse;
            font-size: 12px;
          }

          .rwd-table tr:first-child {
            border-top: none;
            background: white;
            color: black;
          }

          .rwd-table tr {
            border-top: 1px solid white;
            border-bottom: 1px solid #ddd;
            background-color: white;
          }

          .rwd-table tr:nth-child(odd):not(:first-child) {
            background-color: white;
          }

          .rwd-table th {
            display: none;

          }

          .rwd-table td {
            display: block;

          }

          .rwd-table td:first-child {
            margin-top: .1em;
          }

          .rwd-table td:last-child {
            margin-bottom: .1em;
          }

          .rwd-table td:before {
            content: attr(data-th) ": ";
            font-weight: bold;
            width: 120px;
            display: inline-block;
            color: white;
          }

          .rwd-table th,
          .rwd-table td {
            text-align: left;
          }

          .rwd-table {
            color: #333;
            border-radius: .2em;
            overflow: hidden;
          }

          .rwd-table tr {
            border-color: #bfbfbf;
          }

          .rwd-table th,
          .rwd-table td {
            padding: .1em 0.5em;
          }
          @media screen and (max-width: 601px) {
            .rwd-table tr:nth-child(2) {
              border-top: none;
            }
          }
          @media screen and (min-width: 600px) {
            .rwd-table tr:hover:not(:first-child) {
              background-color: #d8e7f3;
            }
            .rwd-table td:before {
              display: none;
            }
            .rwd-table th,
            .rwd-table td {
              display: table-cell;
              padding: .10em .3em;
            }
            .rwd-table th:first-child,
            .rwd-table td:first-child {
              padding-left: 0;
            }
            .rwd-table th:last-child,
            .rwd-table td:last-child {
              padding-right: 0;
            }
            .rwd-table th,
            .rwd-table td {
              padding: 0.5em !important;
            }
          }
    </style>
</head>
    <body style="margin: 0px">
<table border="0" style="width: 100%">
    <tr>
        <td style="text-align:30px; padding-right: -40px"><img style="width:80px;" src="images/logounisma.png"></td>
        <td style="text-align: center">
            <h3>LAPORAN SURAT MASUK<BR>
                UNIVERSITAS ISLAM MALANG</h3>
            <hr style="margin-bottom: -22px">
           <b style="font-size: 11px;"><i>Laporan Surat Masuk 2020</i></b><br>

        </td>

    </tr>
</table>

        <br>
        <table class="rwd-table" border="1" style="border-color:grey;width: 100%; font-size:10px">
            <tr>
                <TH >NO</TH>
                <TH style="text-align: center"> Tgl Surat</TH>
                <TH style="text-align: center"> Jenis</TH>
                <TH style="text-align: center"> Nomor Surat</TH>
                <TH style="text-align: center"> Perihal</TH>

            </tr>
            @php
                $no = 1;
            @endphp

            @foreach ($laporan as $key => $item)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$item->tgl_surat}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->nomor_surat}}</td>
                    <td>{{$item->perihal}}</td>
                </tr>

            @endforeach

        </table>
        <br>

            <br>


    </body>
</html>
