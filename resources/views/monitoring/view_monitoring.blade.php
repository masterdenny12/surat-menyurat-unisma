@extends('master_sub')
@section('konten')


        <div class="row">

            <div class="col-xl-4 col-lg-12">

        <!--Begin::Portlet-->
        <div class="m-portlet m-portlet--full-height ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Detail Surat
                        </h3>
                    </div>
                </div>

            </div>
            <div class="m-portlet__body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">Tanggal</label>
                        </div>
                        <div class="col-md-7">
                           <label>: <mark><b>{{$get_surat->tgl_surat}}</b></mark></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">Kategori</label>
                        </div>
                        <div class="col-md-7">
                           <label>: {{$get_surat->kategori}}</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">Jenis Surat</label>
                        </div>
                        <div class="col-md-7">
                           <label>: <b>{{$get_surat->nama}}</b></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">Nomor Surat</label>
                        </div>
                        <div class="col-md-7">
                           <label>: {{$get_surat->nomor_surat}}</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">Perihal</label>
                        </div>
                        <div class="col-md-7">
                           <label>: {{$get_surat->perihal}}</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">Dari</label>
                        </div>
                        <div class="col-md-7">
                           <label>: <b>{{$get_surat->dari}}</b></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">Kepada</label>
                        </div>
                        <div class="col-md-7">
                           <label>: {{$get_surat->nama_jabatan}}</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">File Surat</label>
                        </div>
                        <div class="col-md-7">
                            : <a href="#" data-toggle="modal"
                            data-target="#modal_show_file" class="m-link m-link--brand m--font-bolder">{{$get_surat->file_upload}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--End::Portlet-->
    </div>
    <div class="col-xl-8 col-lg-12">
        <div class="m-portlet m-portlet--full-height ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Monitoring Disposisi
                        </h3>
                    </div>
                </div>

            </div>
        <div class="m-portlet__body">

            <div class="container-fluid">
                <div class="col-sm-12 col-md-6">
                  <div id="jstree">
                    <ul>
                        @foreach ($result as $key => $item)

                         <li class="select-entity" data-jstree='{"icon":"fa fa-user"}'><b>{{$item[0]->jabatan_mendisposisi}}</b>
                             @foreach ($item as $a)
                           <ul>
                               <li class="select-entity" data-jstree='{"icon":"fa fa-user"}'><a href="#" data-tgl_disposisi="{{$a->tgl_disposisi}}"
                                   data-jam_disposisi="{{$a->jam_disposisi}}"
                                   data-isi_disposisi="{{$a->isi_disposisi}}"
                                   data-opsi="{{$a->opsi}}"
                                   data-jabatan_mendisposisi="{{$a->jabatan_mendisposisi}}"
                                   data-jabatan_disposisi="{{$a->jabatan_disposisi}}"
                                   data-toggle="modal" data-target="#modal-detail"> {{$a->jabatan_disposisi}}</a></li>
                            </ul>
                         @endforeach

                         </li>

                        @endforeach


                    </ul>
                  </div>
                </div>
              </div>
        </div>
        </div>
    </div>
        </div>

        <div class="modal fade" id="modal_show_file">
            <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 50%;overflow-y: initial !important">
                <div class="modal-content">
                    <!-- Modal Header -->

                    <!-- Modal body -->
                    <div class="modal-body" style="height: 480px; overflow-y: auto;">
                        @php
                            $url = '../FILEUPLOAD/'.$get_surat->file_upload;

                        @endphp
                        <embed src="{{$url}}" type = "application / pdf" width = "100%" height = "450px" />
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer" style="height: 60px">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-detail">
            <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 40%;overflow-y: initial !important">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header" style="background-color: #4f7f4f;color:white">
                        <h6 class="modal-title" style="color: whitesmoke;height:5px;padding-bottom:7px;margin-top:-10px"><b><i class="m-menu__link-icon flaticon-mail"></i> Detail Disposisi</b></h6>
                        <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body" style="height: 350px;
                    overflow-y: auto;">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                            <strong>Success!</strong>Product was added successfully.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <form id="form_detail" enctype="multipart/form-data" method="post">
                            {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4"> <label for="Name">Disposisi Oleh</label>
                                        </div>
                                        <div class="col-md-8">
                                            <b id="monitoring_disposisi_oleh"></b>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4"> <label for="Name">Disposisi Ke</label>
                                        </div>
                                        <div class="col-md-8">
                                            <b id="monitoring_disposisi_ke"></b>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4"> <label for="Name">Tgl Disposisi</label>
                                        </div>
                                        <div class="col-md-8">
                                            <p id="monitoring_tgl_disposisi"></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4"> <label for="Name">Jam Disposisi</label>
                                        </div>
                                        <div class="col-md-8">
                                            <p id="monitoring_jam_disposisi"></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Isi Disposisi</label>
                                        </div>
                                        <div class="col-md-8">
                                            <p id="monitoring_isi_disposisi"></p>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Opsi Disposisi</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div id="monitoring_opsi"></div>

                                        </div>
                                    </div>
                                </div>

                            </div>



                        </div>


                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer" style="height: 60px">
                        <button type="button" class="btn btn-danger btn-md m-btn m-btn m-btn--icon" data-dismiss="modal"><span>
                            <i class="la la-close"></i>
                            <span>Keluar</span>
                        </span></button>
                    </div>
                </form>
                </div>
            </div>
        </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script>

$(document).ready(function() {
    $('#html').jstree({
		"core" : {
			"animation" : 5,
			"check_callback" : true,
			"themes" : {
				"variant" : "large"
			}
		},
		"types" : {

			"root" : {
			  "valid_children" : ["default"]
			},
			"default" : {
			  "valid_children" : ["default","file"]
			},
			"file" : {
				"icon" : "jstree-file",
				"valid_children" : []
			}
		},
		"plugins" : [
			"contextmenu", "dnd", "types", "state"
		]
	});

    $(function() {
      //invoke the method jstree();
      $("#jstree").jstree();
      // bind to events triggered on the tree
      $("#jstree").on("changed.jstree", function(e, data) {
      });
    });

    {{--  $('#btn_detail_montoring').on('click', function (e) {
        console.log('oke');
        var id_surat = $(this).data('id_surat');
        var id_jabatan = $(this).data('id_jabatan');
        var id_usrz = $(this).data('id_usrz');
        console.log(id_surat);
        var url = '{!! url('getSuratMonitoring') !!}/'+id_surat + '/' + id_jabatan + '/' + id_usrz;
        ajaxDetailSurat(url);
    });  --}}

    modal_detail();

        function modal_detail() {
            $("#modal-detail").on('show.bs.modal', function (event) {

                var button = $(event.relatedTarget) // Button that triggered the modal
                var tgl_disposisi = button.data('tgl_disposisi');
                var jam_disposisi = button.data('jam_disposisi');
                var isi_disposisi = button.data('isi_disposisi');
                var opsi = button.data('opsi');
                var jabatan_mendisposisi = button.data('jabatan_mendisposisi');
                var jabatan_disposisi = button.data('jabatan_disposisi');

                $("#monitoring_tgl_disposisi").html(': '+ tgl_disposisi);
                $("#monitoring_jam_disposisi").html(': '+jam_disposisi);
                $("#monitoring_isi_disposisi").html(': '+isi_disposisi);
                $("#monitoring_opsi").html(': '+opsi);
                $("#monitoring_disposisi_oleh").html(': ' + jabatan_mendisposisi);
                $("#monitoring_disposisi_ke").html(': '+ jabatan_disposisi);
            });
        }


  });

  </script>
@endsection
