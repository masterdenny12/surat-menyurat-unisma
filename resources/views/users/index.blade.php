@extends('master')

@section('konten')
<head>
    <style>
        .modal.modal-wide{
            overflow: hidden;
        }

        .modal-wide .modal-body {
            overflow-y: auto;
        }
    </style>
</head>
<div class="m-grid__item m-grid__item--fluid m-wrapper" style="margin: 10px">
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <div class="row">
                        <div class="col-md-12"><h5><i class="m-menu__link-icon flaticon-users"></i> Data Users</h5></div>

                    </div>
                </h3>
            </div>
        </div>

        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" data-toggle="modal" data-target="#CreateUserModal" class="btn btn-primary m-btn m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah User</span>
                        </span>
                    </a>

                </li>
            </ul>
        </div>
    </div>

    <div class="m-portlet__body">
        <table class="table dataTable tabel_show" style="width: 100%">
            <thead style="background-color: #f6f6f6;color:#726363">
                <tr>
                    <th>No.</th>
                    <th>name</th>
                    <th>username</th>
                    <th>email</th>
                    <th>Aksi</th>

                </tr>
            </thead>
        </table>
    </div>
    {{--  input data modal  --}}
    <div class="modal fade" id="CreateUserModal">
        <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 50%;overflow-y: initial !important">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="background-color: #4f7f4f;color:white">
                    <h6 class="modal-title" style="color: whitesmoke;height:5px;padding-bottom:7px;margin-top:-10px"><b><i class="m-menu__link-icon flaticon-user"></i> Input User</b></h6>
                    <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" style="height: 390px;
                overflow-y: auto;">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success!</strong>Product was added successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <b style="color: red">*Form Users Baru</b>
                    <hr>
                    <form id="form_submit" method="post">
                        @csrf
                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">* Hak Akses:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="role_id" id="txtinput_role" required
                                        class="custom-select select2 form-control hak_akses" style="width:100%">
                                            <option selected disabled>Pilih Hak Akses..</option>
                                            <option class="c_user" value="4">User Input Surat</option>
                                            <option class="c_pejabat" value="2">User Pejabat</option>
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group catatan">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">* Jabatan:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="jabatan_id" id="txtinput_id_jabatan"
                                        class="custom-select select2 form-control" style="width:100%">
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name"* >Nama Lengkap:</label>
                                    </div>
                                    <div class="col-md-8">
                                       <input required type="text" class="form-control" placeholder="Nama lengkap.." name="name">

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">* Username:</label><br>
                                        <small style="color: red">Username untuk login !</small>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="text" required class="form-control" placeholder="Username.." name="username">

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">* Password:</label><br>
                                        <small style="color: red">Password untuk login !</small>
                                    </div>
                                    <div class="col-md-8">
                                       <input required type="password" class="form-control" placeholder="Password.." name="password">

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Email:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input name="email"
                                        class="form-control" placeholder="Email" style="width:100%">
                                        </input>

                                    </div>
                                </div>
                            </div>



                        </div>


                    </div>


                </div>
                <!-- Modal footer -->
                <div class="modal-footer" style="height: 70px">
                    <button type="submit" class="btn btn-success" id="SubmitCreateForm">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                </div>
            </form>
            </div>
        </div>
    </div>

    {{--  edit data modal  --}}
    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 50%;overflow-y: initial !important">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="background-color: #4f7f4f;color:white">
                    <h6 class="modal-title" style="color: whitesmoke;height:5px;padding-bottom:7px;margin-top:-10px"><b><i class="m-menu__link-icon flaticon-user"></i> Update User</b></h6>
                    <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" style="height: 420px;
                overflow-y: auto;">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success!</strong>Product was added successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <b style="color: red">*Form Ubah User</b>
                    <hr>
                    <form id="form_edit" enctype="multipart/form-data" method="post">
                        {!! csrf_field() !!}
                        <input type="hidden" id="txtedit_id"  class="form-control" name="id">
                        <div class="row">

                            <div class="col-md-12">

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">* Hak Akses:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="role_id" id="txtedit_role" required
                                            class="custom-select select2 form-control" style="width:100%">
                                            <option selected disabled>Pilih Hak Akses..</option>
                                            <option value="4">User Input Surat</option>
                                            <option value="2">User Pejabat</option>
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">* Jabatan:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="jabatan_id" id="txtedit_id_jabatan"
                                            class="custom-select select2 form-control" style="width:100%">
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name"* >Nama Lengkap:</label>
                                        </div>
                                        <div class="col-md-8">
                                           <input required type="text" id="txtedit_name" class="form-control" placeholder="Nama lengkap.." name="name">

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">* Username:</label><br>
                                            <small style="color: red">Username untuk login !</small>
                                        </div>
                                        <div class="col-md-8">
                                           <input type="text" required class="form-control" id="txtedit_username" placeholder="Username.." name="username">

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">* Password:</label><br>
                                            <small style="color: red">Password untuk login !</small>
                                        </div>
                                        <div class="col-md-8">
                                           <input required type="password"  class="form-control" placeholder="Password.." name="password">

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Email:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input name="email"
                                            class="form-control" id="txtedit_email" placeholder="Email" style="width:100%">
                                            </input>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>


                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" id="SubmitCreateForm">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                </div>
            </form>
            </div>
        </div>
    </div>

</div>
</div>
    {{--  modal detail  --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="https://mozilla.github.io/pdf.js/build/pdf.js"></script>

<script>

    $(document).ready(function () {

        $('.catatan').fadeOut();
        $( ".hak_akses" ).change(function() {
            if($(this).val() == 2){
                $('.catatan').fadeIn();
            }else{
                $('.catatan').fadeOut();
            }
          });

        $('#txtinput_role').select2({
            placeholder: 'Hak akses..',
            dropdownParent: $("#txtinput_role").parent(),

          });

          $('#txtedit_role').select2({
            placeholder: 'Hak akses..',
            dropdownParent: $("#txtedit_role").parent(),

          });

          $('#txtinput_id_jabatan').select2({
            placeholder: 'Jabatan...',
            dropdownParent: $("#txtinput_id_jabatan").parent(),
            ajax: {
              url: '{{url("/cari/jabatan")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nama_jabatan,
                      id: item.id_jabatan
                    }
                  })
                };
              },
              cache: true,
            }
          });


          $('#txtedit_id_jabatan').select2({
            placeholder: 'Jabatan...',
            dropdownParent: $("#txtedit_id_jabatan").parent(),
            ajax: {
              url: '{{url("/cari/jabatan")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nama_jabatan,
                      id: item.id_jabatan
                    }
                  })
                };
              },
              cache: true,
            }
          });


        var table_users = $('.tabel_show').DataTable({
            responsive:true,
            paging: true,
            info: true,
            searching: true,
            "aaSorting": [],
            "ordering": true,
            ajax: {
                url: '{{url("show_datatable_users")}}',
                dataSrc: 'result',
            },

        });

        //input data

        $('#form_submit').submit(function (event) {
            event.preventDefault();
            $('#CreateUserModal').modal('hide');
            var url = '{{ url('simpan_users') }}';
            ajaxProcessInsert(url, new FormData(this))
        });

        function ajaxProcessInsert(url, data) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    if (response) {
                        console.log(response);
                        $('.cssload-container').hide();
                        $('#form_submit')[0].reset();
                        $('.tabel_show').DataTable().ajax.reload();
                        Swal.fire({
                            title: 'Information',
                            html: 'User berhasil disimpan',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.errors;
                            if (Array.isArray(message)) {
                                console.log(message);
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };

        //action edit

        $('#form_edit').submit(function (event) {
            $('#modal-edit').modal('hide');
            event.preventDefault();
            var url = '{{ url('update_users') }}';
            ajaxProcess(url, $(this).serialize())
        });

        function ajaxProcess(url, data) {
            $.ajax({
                type: 'post',
                url: url,
                data: data,
                dataType : 'json',
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    console.log(response);
                    if (response) {
                        $('.cssload-container').hide();

                        $('.tabel_show').DataTable().ajax.reload();


                        Swal.fire({
                            title: 'Information',
                            html: '<strong>' + response.result + '</strong>',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('#form_edit')[0].reset();
                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };

        $('.tabel_show tbody').on('click', '#btn_update_surat', function (e) {
            var id = $(this).attr('data-id');
            var url = '{!! url('get_user') !!}/'+id;

            ajaxEditSurat(url);
        });

        function ajaxEditSurat(url){
            $.ajax({
                type: 'GET',
                url: url,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    $('.cssload-container').hide();
                    if(response.status == 'success') {
                       if(response.result != null){
                           var data = response.result;
                           $('#txtedit_id').val(data.id);
                           $('#txtedit_name').val(data.name);
                           $('#txtedit_username').val(data.username);
                           $('#txtedit_email').val(data.email);

                           var selected_id_jabatan = $("<option selected='selected'></option>").val(data.jabatan_id).text(data.nama_jabatan);
                           $("#txtedit_id_jabatan").append(selected_id_jabatan).trigger('change');
                           $('#txtedit_role').val(data.role_id).trigger('change');
                           $('#modal-edit').modal('show');
                       }
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        }

        //modal detail jquery

        //delete data propinsi

        $('.tabel_show tbody').on('click', '#btn-delete', function () {
            var url = '{{ url('destroy_users') }}';
            var id = $(this).attr('data-id');
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var data_id = {_token: CSRF_TOKEN, id: id};
            delete_master(url, data_id);
        });


        function delete_master(url, data) {
            swal.fire({
                title: "Informasi !",
                text: "Apakah anda ingin menghapus data ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Ya, Hapus data!'
            }).then((result) => {
                if(result.value)
                {
                    $.ajax({
                    type: "POST",
                    url: url,
                    /* send the csrf-token and the input to the controller */
                    data: data,
                    dataType: "json",
                    /* remind that 'data' is the response of the AjaxController */
                    beforeSend: function () {
                        $('.cssload-container').show();
                    },
                    success: function (data) {
                        $('.cssload-container').hide();
                        $('.tabel_show').DataTable().ajax.reload();

                        swal.fire({
                            title: "Delete!",
                            text: data.result,
                            type: data.status,
                            button: false,
                            timer: 2000,
                        })
                    },
                    error: function (error) {
                        if (error) {
                            console.log(error);
                            var result = error.responseJSON;
                            if (result != null) {
                                var message = result.message;
                                if (Array.isArray(message)) {
                                    $.map(message, function (value, index) {
                                        message = value + '</br>';
                                    });
                                }
                            } else {
                                message = 'look like something when wrong';
                            }
                        } else {
                            message = 'look like something when wrong';
                        }

                        $('.cssload-container').hide();
                        Swal.fire({
                            title: 'Warning',
                            html: '<strong>' + message + '</strong>',
                            type: 'warning',
                            showConfirmButton: false,
                            timer: 3000
                        })

                    }

                });
                }
            });
        }

    })
</script>

@endsection
