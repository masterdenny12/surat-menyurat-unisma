@extends('master')

@section('konten')
<head>
    <style>
        .modal.modal-wide{
            overflow: hidden;
        }

        .modal-wide .modal-body {
            overflow-y: auto;
        }
    </style>
</head>
<div class="m-grid__item m-grid__item--fluid m-wrapper" style="margin: 10px">
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <div class="row">
                        <div class="col-md-12"><h5><i class="m-menu__link-icon flaticon-mail"></i> Data Surat Masuk</h5></div>

                    </div>
                </h3>
            </div>
        </div>
        @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 4 )
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="#" data-toggle="modal" data-target="#CreateSuratModal" class="btn btn-primary m-btn m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Tambah Surat Masuk</span>
                            </span>
                        </a>

                    </li>
                </ul>
            </div>
        @endif
    </div>

    <div class="m-portlet__body">
        <table class="table dataTable tabel_show" style="width: 100%">
            <thead style="background-color: #f6f6f6;color:#726363">
                <tr>
                    <th>No.</th>
                    <th style="width: 12%">Tgl Surat</th>
                    <th>Jenis</th>
                    <th style="width: 15%">Nomor surat</th>
                    <th>Perihal</th>
                    @if (Auth::user()->role_id == 3 || Auth::user()->role_id == 4)
                    <th>ditolak</th>
                    @endif
                    <th>Status</th>
                    @if (Auth::user()->role_id == 4)
                    <th style="width: 34%">Aksi</th>
                    @else
                    <th style="width: 30%">Aksi</th>
                    @endif

                </tr>
            </thead>

        </table>
    </div>
    {{--  input data modal  --}}
    <div class="modal fade" id="CreateSuratModal">
        <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 90%;overflow-y: initial !important">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="background-color: #4f7f4f;color:white">
                    <h6 class="modal-title" style="color: whitesmoke;height:5px;padding-bottom:7px;margin-top:-10px"><b><i class="m-menu__link-icon flaticon-mail"></i> Input Surat Masuk</b></h6>
                    <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" style="height: 420px;
                overflow-y: auto;">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success!</strong>Product was added successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <b style="color: red">*Form Input Surat Masuk</b>
                    <hr>
                    <form id="form_submit" method="post">
                        @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4"> <label for="Name">Tanggal Surat</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="input-group m-input-group m-input-group--square">
                                            <input type="text" name="tgl_surat" id="txtinput_tgl_surat" placeholder="Tanggal Surat" class="form-control" autocomplete="off">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4"> <label for="Name">Tanggal Surat Terima</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="input-group m-input-group m-input-group--square">
                                            <input type="text" name="tgl_surat_terima" id="txtinput_tgl_surat_terima" placeholder="Tanggal Surat Terima" class="form-control" autocomplete="off">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Jenis Surat:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="id_jenis" id="txtinput_jenis_surat"
                                        class="custom-select select2 form-control" style="width:100%">
                                        </select>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Kategori Surat:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="id_kategori" id="txtinput_kategori_surat"
                                        class="custom-select select2 form-control" style="width:100%">
                                        </select>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Nomor Surat:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input name="nomor_surat" id="txtinput_nomor_surat"
                                        class="form-control" placeholder="Nomor Surat" style="width:100%">
                                        </input>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Nomor agenda:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input name="nomor_agenda" id="txtinput_nomor_agenda"
                                        class="form-control" placeholder="Nomor Agenda" style="width:100%">
                                        </input>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Kode Arsip:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="id_kode_arsip" id="txtinput_id_kode_arsip"
                                        class="custom-select select2 form-control" style="width:100%">

                                        </select>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Kepada:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="id_jabatan" id="txtinput_id_jabatan"
                                        class="custom-select select2 form-control" style="width:100%">
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Perihal:</label>
                                    </div>
                                    <div class="col-md-8">
                                       <textarea class="form-control" name="perihal" placeholder="Perihal..."></textarea>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Dari:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input name="dari" id="txtinput_dari"
                                        class="form-control" placeholder="Dari..." style="width:100%">
                                        </input>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Catatan:</label>
                                    </div>
                                    <div class="col-md-8">
                                       <textarea class="form-control" name="catatan" placeholder="Catatan..."></textarea>

                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Upload File Surat:</label><br>
                                        <small style="color: red"><b>* Format upload JPG atau PDF</b></small>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="file" name="file_upload" id="myPdf" accept="image/PNG, image/png, image/jpg,image/JPG, image/jpeg, image/JPEG"/><br>
                                        <canvas style="width: 70%" id="pdfViewer"></canvas>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>


                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" id="SubmitCreateForm">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                </div>
            </form>
            </div>
        </div>
    </div>

    {{--  edit data modal  --}}
    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 90%;overflow-y: initial !important">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="background-color: #4f7f4f;color:white">
                    <h6 class="modal-title" style="color: whitesmoke;height:5px;padding-bottom:7px;margin-top:-10px"><b><i class="m-menu__link-icon flaticon-mail"></i> Input Surat Masuk</b></h6>
                    <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" style="height: 420px;
                overflow-y: auto;">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success!</strong>Product was added successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <b style="color: red">*Form Input Surat Masuk</b>
                    <hr>
                    <form id="form_edit" enctype="multipart/form-data" method="post">
                        {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4"> <label for="Name">Tanggal Surat</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="hidden" name="id_surat" id="txtedit_id_surat">
                                        <div class="input-group m-input-group m-input-group--square">
                                            <input type="text" name="tgl_surat" id="txtedit_tgl_surat" placeholder="Tanggal Surat" class="form-control" autocomplete="off">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4"> <label for="Name">Tanggal Surat Terima</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="input-group m-input-group m-input-group--square">
                                            <input type="text" name="tgl_surat_terima" id="txtedit_tgl_surat_terima" placeholder="Tanggal Surat Terima" class="form-control" autocomplete="off">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Jenis Surat:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="id_jenis" id="txtedit_jenis_surat"
                                        class="custom-select select2 form-control" style="width:100%">
                                        </select>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Kategori Surat:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="id_kategori" id="txtedit_kategori_surat"
                                        class="custom-select select2 form-control" style="width:100%">
                                        </select>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Nomor Surat:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input name="nomor_surat" id="txtedit_nomor_surat"
                                        class="form-control" placeholder="Nomor Surat" style="width:100%">
                                        </input>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Nomor agenda:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input name="nomor_agenda" id="txteditnomoragenda"
                                        class="form-control" placeholder="Nomor Agenda" style="width:100%">
                                        </input>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Kode Arsip:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="id_kode_arsip" id="txtedit_id_kode_arsip"
                                        class="custom-select select2 form-control" style="width:100%">

                                        </select>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Kepada:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="id_jabatan" id="txtedit_id_jabatan"
                                        class="custom-select select2 form-control" style="width:100%">
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Perihal:</label>
                                    </div>
                                    <div class="col-md-8">
                                       <textarea class="form-control" id="txteditperihal" name="perihal" placeholder="Perihal..."></textarea>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Dari:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input name="dari" id="txteditdari"
                                        class="form-control" placeholder="Dari..." style="width:100%">
                                        </input>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Catatan:</label>
                                    </div>
                                    <div class="col-md-8">
                                       <textarea class="form-control" id="txteditcatatan" name="catatan" placeholder="Catatan..."></textarea>

                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Upload File Surat:</label><br>
                                        <small style="color: red"><b>* Format upload JPG atau PDF</b></small>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="file" name="file_upload" class="file_upload_edit"/><br>
                                        <br>
                                        <a href="#" data-toggle="modal"
                                        data-target="#modal_show_file_edit" class="m-link m-link--brand m--font-bolder"><div id="txtedit_file_upload"></div></a>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>


                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" id="SubmitCreateForm">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                </div>
            </form>
            </div>
        </div>
    </div>


    {{--  modal detail  --}}

    <div class="modal fade" id="modal-detail">
        <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 70%;overflow-y: initial !important">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="background-color: #4f7f4f;color:white">
                    <h6 class="modal-title" style="color: whitesmoke;height:5px;padding-bottom:7px;margin-top:-10px"><b><i class="m-menu__link-icon flaticon-mail"></i> Detail Surat Masuk</b></h6>
                    <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" style="height: 400px;
                overflow-y: auto;">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success!</strong>Product was added successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form id="form_detail" enctype="multipart/form-data" method="post">
                        {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6"> <label for="Name">Tanggal Surat</label>
                                    </div>
                                    <div class="col-md-6">
                                        <b id="detail_tgl_surat"></b>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6"> <label for="Name">Tanggal Surat Terima</label>
                                    </div>
                                    <div class="col-md-6">
                                        <b id="detail_tgl_surat_terima"></b>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="Name">Jenis Surat</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p id="detail_jenis_surat"></p>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="Name">Kategori Surat</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="detail_kategori_surat"></div>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="Name">Nomor Surat</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="detail_nomor_surat"></div>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="Name">Nomor agenda</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="detail_nomor_agenda"></div>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="Name">Kode Arsip</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="detail_id_kode_arsip"></div>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="Name">Perihal</label>
                                    </div>
                                    <div class="col-md-6">
                                       <div id="detail_perihal"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="Name">Dari</label>
                                    </div>
                                    <div class="col-md-9">
                                       <div id="detail_dari"></div>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="Name">Catatan</label>
                                    </div>
                                    <div class="col-md-9">
                                       <div id="detail_catatan"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="Name">Kepada</label>
                                    </div>
                                    <div class="col-md-9">
                                       <div id="detail_jabatan"></div>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="Name">Opsi</label>
                                    </div>
                                    <div class="col-md-9">
                                       <div id="detail_opsi"></div>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="Name">File Surat:</label><br>

                                    </div>
                                    <div class="col-md-9">
                                        <a href="#" data-toggle="modal"
                                        data-target="#modal_show_file" class="m-link m-link--brand m--font-bolder"><div id="detail_file_upload"></div></a>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>


                </div>
                <!-- Modal footer -->
                <div class="modal-footer" style="height: 60px">

                    <a id="click_print" target="_blank" class="btn btn-success btn-md m-btn  m-btn m-btn--icon">
                    <span>
                        <i class="la la-print"></i>
                        <span>Print</span>
                    </span>
                    </a>
                    <a  id="click_download" class="btn btn-primary btn-md m-btn  m-btn m-btn--icon" download>
                        <span>
                            <i class="la la-download"></i>
                            <span>Download</span>
                        </span>
                    </a>

                    <button type="button" class="btn btn-danger btn-md m-btn m-btn m-btn--icon" data-dismiss="modal"><span>
                        <i class="la la-close"></i>
                        <span>Keluar</span>
                    </span></button>
                </div>
            </form>
            </div>
        </div>
    </div>


</div>

</div>

<div class="modal fade" id="modal_show_file">
    <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 50%;overflow-y: initial !important">
        <div class="modal-content">
            <!-- Modal Header -->

            <!-- Modal body -->
            <div class="modal-body" style="height: 480px; overflow-y: auto;">

                <embed id="src_img_pdf" type = "application / pdf" width = "100%" height = "450px" />
            </div>
            <!-- Modal footer -->
            <div class="modal-footer" style="height: 60px">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
            </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_show_file_edit">
    <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 50%;overflow-y: initial !important">
        <div class="modal-content">
            <!-- Modal Header -->

            <!-- Modal body -->
            <div class="modal-body" style="height: 480px; overflow-y: auto;">

                <embed id="src_img_pdf_edit" type = "application / pdf" width = "100%" height = "450px" />
            </div>
            <!-- Modal footer -->
            <div class="modal-footer" style="height: 60px">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
            </div>
        </form>
        </div>
    </div>
</div>
 {{--  edit data modal  --}}
 <div class="modal fade" id="modal-proses">
    <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 40%;overflow-y: initial !important">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header" style="background-color: #4f7f4f;color:white">
                <h6 class="modal-title" style="color: whitesmoke;height:5px;padding-bottom:7px;margin-top:-10px"><b><i class="m-menu__link-icon flaticon-mail"></i> Approval Surat Masuk</b></h6>
                <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body" style="height: 300px;
            overflow-y: auto;">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                    <strong>Success!</strong>Product was added successfully.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <b style="color: red"><small>*Surat yang sudah diubah tidak bisa diedit kembali !</small></b>
                <hr>
                <form id="form_edit_approve" enctype="multipart/form-data" method="post">
                    {!! csrf_field() !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="Name">Nomor Surat:</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="hidden" readonly name="id_surat_approve" id="approve_surat_id" class="form-control">
                                  <input type="text" readonly name="nomor_surat" id="approve_nomor_surat" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="Name">Proses Surat:</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="m-demo__preview">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-secondary active">
                                                <input type="radio" class="c_diproses" name="approve_status_surat" id="option1" autocomplete="off" value="0" checked=""> Proses
                                            </label>
                                            <label class="btn btn-secondary">
                                                <input type="radio" class="c_diterima" name="approve_status_surat" id="option2" autocomplete="off" value="1"> Diterima
                                            </label>
                                            <label class="btn btn-secondary">
                                                <input type="radio" class="c_ditolak" name="approve_status_surat" id="option3" autocomplete="off" value="2"> Ditolak
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-group catatan">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="Name">Catatan:</label>
                                </div>
                                <div class="col-md-8">
                                   <textarea class="form-control" rows="3" name="catatan_approve" placeholder="Beri catatan disini..."></textarea>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
            <!-- Modal footer -->
            <div class="modal-footer" style="height: 70px">
                <button type="submit" class="btn btn-primary" id="SubmitCreateForm">Proses</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
            </div>
        </form>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="https://mozilla.github.io/pdf.js/build/pdf.js"></script>

<script>

    $(document).ready(function () {

        $('.catatan').fadeOut();
        $(".c_ditolak").change(function() {
            $(".catatan").fadeIn();
        });
        $(".c_diterima").change(function() {
            $(".catatan").fadeOut();
        });
        $(".c_diproses").change(function() {
            $(".catatan").fadeOut();
        });
        $('#form_edit_approve').submit(function (event) {
            $('#modal-proses').modal('hide');
            event.preventDefault();
            var url = '{{ url('update_approve') }}';
            ajaxProcessApprove(url, $(this).serialize())
        });

        function ajaxProcessApprove(url, data) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    if (response.status == 'success') {
                        $('.cssload-container').hide();

                        $('.tabel_show').DataTable().ajax.reload();


                        Swal.fire({
                            title: 'Information',
                            html: '<strong>' + response.result + '</strong>',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                    $('#form_edit_approve')[0].reset();
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }



                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };

        modal_edit_approve();

        function modal_edit_approve() {
            $('#modal-proses').on('show.bs.modal', function (event) {
                // do something...
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id');
                var nomor_surat = button.data('nomor_surat');
                $('#approve_surat_id').val(id);
                $('#approve_nomor_surat').val(nomor_surat);
            });
        }

        var pdfjsLib = window['pdfjs-dist/build/pdf'];
// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = 'https://mozilla.github.io/pdf.js/build/pdf.worker.js';

$("#myPdf").on("change", function(e){
	var file = e.target.files[0]
	if(file.type == "application/pdf"){
		var fileReader = new FileReader();
		fileReader.onload = function() {
			var pdfData = new Uint8Array(this.result);
			// Using DocumentInitParameters object to load binary data.
			var loadingTask = pdfjsLib.getDocument({data: pdfData});
			loadingTask.promise.then(function(pdf) {
			  console.log('PDF loaded');

			  // Fetch the first page
			  var pageNumber = 1;
			  pdf.getPage(pageNumber).then(function(page) {
				console.log('Page loaded');

				var scale = 1.5;
				var viewport = page.getViewport({scale: scale});

				// Prepare canvas using PDF page dimensions
				var canvas = $("#pdfViewer")[0];
				var context = canvas.getContext('2d');
				canvas.height = viewport.height;
				canvas.width = viewport.width;

				// Render PDF page into canvas context
				var renderContext = {
				  canvasContext: context,
				  viewport: viewport
				};
				var renderTask = page.render(renderContext);
				renderTask.promise.then(function () {
				  console.log('Page rendered');
				});
			  });
			}, function (reason) {
			  // PDF loading error
			  console.error(reason);
			});
		};
		fileReader.readAsArrayBuffer(file);
	}
});

        $('#txtedit_tgl_surat').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });
        $("#foto_pegawai").change(function(){
            readURL(this);
        });


        $('#txtinput_tgl_surat_terima').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });

        $('#txtedit_tgl_surat_terima').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });

        $('#txtinput_tgl_surat').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });

        $('#txtedit_tgl_surat').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });

        $('#txtinput_jenis_surat').select2({
            placeholder: 'Jenis Surat...',
            dropdownParent: $("#txtinput_jenis_surat").parent(),
            ajax: {
              url: '{{url("/cari/jenis_surat")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nama,
                      id: item.id_jenis
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#txtedit_jenis_surat').select2({
            placeholder: 'Jenis Surat...',
            dropdownParent: $("#txtedit_jenis_surat").parent(),
            ajax: {
              url: '{{url("/cari/jenis_surat")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nama,
                      id: item.id_jenis
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#txtinput_id_jabatan').select2({
            placeholder: 'Jabatan...',
            dropdownParent: $("#txtinput_id_jabatan").parent(),
            ajax: {
              url: '{{url("/cari/jabatan")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nama_jabatan,
                      id: item.id_jabatan
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#txtinput_id_kode_arsip').select2({
            placeholder: 'Kode Arsip...',
            dropdownParent: $("#txtinput_id_kode_arsip").parent(),
            ajax: {
              url: '{{url("/cari/kode_arsip")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.kode_arsip + ' - ' + item.keterangan,
                      id: item.id_kode_arsip
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#txtedit_id_kode_arsip').select2({
            placeholder: 'Kode Arsip...',
            dropdownParent: $("#txtedit_id_kode_arsip").parent(),
            ajax: {
              url: '{{url("/cari/kode_arsip")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.kode_arsip + ' - ' + item.keterangan,
                      id: item.id_kode_arsip
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#txtedit_id_jabatan').select2({
            placeholder: 'Jabatan...',
            dropdownParent: $("#txtedit_id_jabatan").parent(),
            ajax: {
              url: '{{url("/cari/jabatan")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nama_jabatan,
                      id: item.id_jabatan
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#txtinput_kategori_surat').select2({
            placeholder: 'Kategori Surat...',
            dropdownParent: $("#txtinput_kategori_surat").parent(),
            ajax: {
              url: '{{url("/cari/kategori_surat")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.kategori,
                      id: item.id_kategori
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#txtedit_kategori_surat').select2({
            placeholder: 'Kategori Surat...',
            dropdownParent: $("#txtedit_kategori_surat").parent(),
            ajax: {
              url: '{{url("/cari/kategori_surat")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.kategori,
                      id: item.id_kategori
                    }
                  })
                };
              },
              cache: true,
            }
          });

        var table_propinsi = $('.tabel_show').DataTable({
            responsive:true,
            paging: true,
            info: true,
            searching: true,
            "aaSorting": [],
            "ordering": true,
            ajax: {
                url: '{{url("show_datatable")}}',
                dataSrc: 'result',
            },


        });


        //input data

        $('#form_submit').submit(function (event) {
            event.preventDefault();
            $('#CreateSuratModal').modal('hide');
            var url = '{{ url('simpan_surat_masuk') }}';
            ajaxProcessInsert(url, new FormData(this))
        });

        function ajaxProcessInsert(url, data) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    if (response) {
                        console.log(response);
                        $('.cssload-container').hide();
                        $('#form_submit')[0].reset();
                        $('.tabel_show').DataTable().ajax.reload();
                        Swal.fire({
                            title: 'Information',
                            html: 'Data berhasil disimpan',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.errors;
                            if (Array.isArray(message)) {
                                console.log(message);
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };

        //action edit

        $('#form_edit').submit(function (event) {
            $('#modal-edit').modal('hide');
            event.preventDefault();
            var url = '{{ url('update_surat_masuk') }}';
            ajaxProcess(url, new FormData(this));
        });

        function ajaxProcess(url, data) {
            $.ajax({
                type: 'post',
                url: url,
                data: data,
                dataType : 'json',
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    console.log(response);
                    if (response) {
                        $('.cssload-container').hide();

                        $('.tabel_show').DataTable().ajax.reload();


                        Swal.fire({
                            title: 'Information',
                            html: '<strong>' + response.result + '</strong>',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('#form_edit')[0].reset();
                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };

        $('.tabel_show tbody').on('click', '#btn_update_surat', function (e) {
            var suratid = $(this).attr('data-suratid');
            var url = '{!! url('get_surat') !!}/'+suratid;

            ajaxEditSurat(url);
        });

        function ajaxEditSurat(url){
            $.ajax({
                type: 'GET',
                url: url,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    $('.cssload-container').hide();
                    if(response.status == 'success') {
                       if(response.result != null){
                           var data = response.result;
                           console.log(data);
                           $('#txtedit_id_surat').val(data.id_surat);
                           $('#txtedit_nomor_surat').val(data.nomor_surat);
                           $('#txteditcatatan').val(data.catatan);
                           $('#txteditperihal').val(data.perihal);
                           $('#txteditdari').val(data.dari);
                           $('#txteditnomoragenda').val(data.nomor_agenda);
                           $("#txtedit_file_upload").html('<b>File</b> :  '+data.file_upload);
                           $("#src_img_pdf_edit").attr("src", 'FILEUPLOAD'+'/'+data.file_upload);
                           var tgl_surat_reverse = data.tgl_surat;
                           var tgl_surat_terima_reverse = data.tgl_surat_terima;
                           date_tgl_surat = tgl_surat_reverse.split("-").reverse().join("-");
                           date_tgl_surat_terima = tgl_surat_terima_reverse.split("-").reverse().join("-");
                           $('#txtedit_tgl_surat').datepicker('setDate', date_tgl_surat);
                           $('#txtedit_tgl_surat_terima').datepicker('setDate', date_tgl_surat_terima);

                           var selected_jenis_surat = $("<option selected='selected'></option>").val(data.id_jenis).text(data.nama);
                           $("#txtedit_jenis_surat").append(selected_jenis_surat).trigger('change');

                           var selected_kategori_surat = $("<option selected='selected'></option>").val(data.id_kategori).text(data.kategori);
                           $("#txtedit_kategori_surat").append(selected_kategori_surat).trigger('change');

                           var selected_id_jabatan = $("<option selected='selected'></option>").val(data.id_jabatan).text(data.nama_jabatan);
                           $("#txtedit_id_jabatan").append(selected_id_jabatan).trigger('change');

                           var selected_id_kode_arsip = $("<option selected='selected'></option>").val(data.id_kode_arsip).text(data.kode_arsip + '-' + data.keterangan);
                           $("#txtedit_id_kode_arsip").append(selected_id_kode_arsip).trigger('change');

                           $('#modal-edit').modal('show');
                       }
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        console.log(result);
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wronsadg';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        }



        //modal detail jquery

        $('.tabel_show tbody').on('click', '#btn_detail_surat', function (e) {
            var suratid = $(this).attr('data-suratiddetail');
            var url = '{!! url('get_surat') !!}/'+suratid;

            ajaxDetailSurat(url);
        });

        function ajaxDetailSurat(url){
            $.ajax({
                type: 'GET',
                url: url,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    $('.cssload-container').hide();
                    if(response.status == 'success') {
                       if(response.result != null){
                           var data = response.result;
                           console.log(data);
                           $('#detail_id_surat').html(':  '+data.id_surat);
                           $('#detail_nomor_surat').html(':  '+data.nomor_surat);
                           $('#detail_catatan').html(':  '+data.catatan);
                           $('#detail_perihal').html(':  '+data.perihal);
                           $('#detail_dari').html(':  '+data.dari);
                           $('#detail_nomor_agenda').html(':  '+data.nomor_agenda);
                           $('#detail_id_kode_arsip').html(':  '+data.kode_arsip + ' - '+ data.keterangan);
                           var tgl_surat_reverse = data.tgl_surat;
                           var tgl_surat_terima_reverse = data.tgl_surat_terima;
                           date_tgl_surat = tgl_surat_reverse.split("-").reverse().join("-");
                           date_tgl_surat_terima = tgl_surat_terima_reverse.split("-").reverse().join("-");
                           $('#detail_tgl_surat').html(':  '+date_tgl_surat);
                           $('#detail_tgl_surat_terima').html(':  '+date_tgl_surat_terima);
                           $("#detail_jenis_surat").html(':  '+data.nama);
                           $("#detail_kategori_surat").html(':  '+data.kategori);
                           $("#detail_jabatan").html(':  '+'<span style="border-radius:2px" class="m-badge m-badge--secondary"><b>'+data.nama_jabatan+'</b>'+'</span>');
                           $("#detail_file_upload").html(':  '+data.file_upload);
                           $("#detail_opsi").html(': '+ data.opsi);
                           $("#click_print").attr('href', "{{url('print_surat_masuk')}}" + '/' + data.id_surat);
                           $("#click_download").attr('href', "{{url('FILEUPLOAD')}}" + '/'+ data.file_upload);
                           $("#src_img_pdf").attr("src", 'FILEUPLOAD'+'/'+data.file_upload);
                           $('#modal-detail').modal('show');
                       }
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        }


        //delete data propinsi

        $('.tabel_show tbody').on('click', '#btn-delete-surat-masuk', function () {
            var url = '{{ url('destroy_surat_masuk') }}';
            var id = $(this).attr('data-id');
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var data_id = {_token: CSRF_TOKEN, id: id};
            delete_master(url, data_id);
        });


        function delete_master(url, data) {
            swal.fire({
                title: "Informasi !",
                text: "Apakah anda ingin menghapus data ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Ya, Hapus data!'
            }).then((result) => {
                if(result.value)
                {
                    $.ajax({
                    type: "POST",
                    url: url,
                    /* send the csrf-token and the input to the controller */
                    data: data,
                    dataType: "json",
                    /* remind that 'data' is the response of the AjaxController */
                    beforeSend: function () {
                        $('.cssload-container').show();
                    },
                    success: function (data) {
                        $('.cssload-container').hide();
                        $('.tabel_show').DataTable().ajax.reload();

                        swal.fire({
                            title: "Delete!",
                            text: data.result,
                            type: data.status,
                            button: false,
                            timer: 2000,
                        })
                    },
                    error: function (error) {
                        if (error) {
                            console.log(error);
                            var result = error.responseJSON;
                            if (result != null) {
                                var message = result.message;
                                if (Array.isArray(message)) {
                                    $.map(message, function (value, index) {
                                        message = value + '</br>';
                                    });
                                }
                            } else {
                                message = 'look like something when wrong';
                            }
                        } else {
                            message = 'look like something when wrong';
                        }

                        $('.cssload-container').hide();
                        Swal.fire({
                            title: 'Warning',
                            html: '<strong>' + message + '</strong>',
                            type: 'warning',
                            showConfirmButton: false,
                            timer: 3000
                        })

                    }

                });
                }
            });
        }

    })
</script>

@endsection
