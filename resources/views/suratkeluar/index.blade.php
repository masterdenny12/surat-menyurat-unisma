@extends('master')

@section('konten')
<head>
    <style>
        .modal.modal-wide{
            overflow: hidden;
        }

        .modal-wide .modal-body {
            overflow-y: auto;
        }
    </style>
</head>
<div class="m-grid__item m-grid__item--fluid m-wrapper" style="margin: 10px">
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <div class="row">
                        <div class="col-md-12"><h5><i class="m-menu__link-icon flaticon-email"></i> Data Surat keluar</h5></div>

                    </div>
                </h3>
            </div>
        </div>

        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                        <a href="#" data-toggle="modal" data-target="#CreateSuratModal" class="btn btn-primary m-btn m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Tambah Surat</span>
                            </span>
                        </a>&nbsp
                        <a href="#" data-toggle="modal" data-target="#CreateFormatSuratModal" class="btn btn-danger m-btn m-btn--icon m-btn--air">
                            <span>
                                <i class="m-menu__link-icon
                                    flaticon-email"></i>
                                <span>Format Surat</span>
                            </span>
                        </a>
                    @elseif (Auth::user()->role_id == 2 || Auth::user()->role_id == 4)
                        <a href="#" data-toggle="modal" data-target="#CreateSuratModal" class="btn btn-primary m-btn m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Tambah Surat</span>
                            </span>
                        </a>&nbsp
                    @endif


                </li>
            </ul>
        </div>
    </div>

    <div class="m-portlet__body">
        <table class="table dataTable tabel_show" style="width: 100%">
            <thead style="background-color: #f6f6f6;color:#726363">
                <tr>
                    <th>No.</th>
                    <th style="width: 12%">Pembuat</th>
                    <th>Tgl surat</th>
                    <th style="width: 15%">Jenis</th>
                    <th>Nomor</th>
                    <th>Perihal</th>
                    <th style="width: 30%">Aksi</th>

                </tr>
            </thead>
        </table>
    </div>
    {{--  input data modal  --}}
    <div class="modal fade" id="CreateSuratModal">
        <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 95%;overflow-y: initial !important">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="background-color: #4f7f4f;color:white">
                    <h6 class="modal-title" style="color: whitesmoke;height:5px;padding-bottom:7px;margin-top:-10px"><b><i class="m-menu__link-icon flaticon-email"></i> Input Surat Keluar</b></h6>
                    <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" style="height: 420px;
                overflow-y: auto;">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success!</strong>Product was added successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <b style="color: red">*Form Input Surat Keluar</b>
                    <hr>
                    <form id="form_submit" method="post">
                        @csrf
                        <input type="hidden" name="id_suratkel" value="{{$generate}}">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4"> <label for="Name">Tanggal Surat</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="input-group m-input-group m-input-group--square">
                                            <input type="text" name="tgl_suratkel" id="txtinput_tgl_surat" placeholder="Tanggal Surat" class="form-control" autocomplete="off">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Jenis Surat:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="id_jenis" id="txtinput_jenis_surat"
                                        class="custom-select select2 form-control" style="width:100%">
                                        </select>

                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Nomor Surat:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input name="nomor" id="txtinput_nomor_surat"
                                        class="form-control" placeholder="Nomor Surat" style="width:100%">
                                        </input>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Perihal:</label>
                                    </div>
                                    <div class="col-md-8">
                                       <textarea class="form-control" name="perihal" placeholder="Perihal..."></textarea>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">Kepada:</label>
                                    </div>
                                    <div class="col-md-8">
                                       <input class="form-control" name="kepada" placeholder="Kepada..."></input>
                                       <input type="hidden" class="form-control" name="auth_user" value="{{Auth::user()->id}}"></input>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-7">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="Name">Tembusan:</label>
                                    </div>
                                    <div class="col-md-10">
                                        <select name="tembusan[]" placeholder = "tembusan.." id="txtinput_tembusan" multiple="multiple"
                                        class="custom-select select2 form-control" style="width:100%">
                                        </select>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="Name">Format Surat:</label><br>
                                        <small style="color: red"><b>* Pilih Format</b></small>
                                    </div>
                                    <div class="col-md-10">
                                        <select name="file_upload"  id="input_format_surat"
                                        class="custom-select select2 form-control" style="width:100%">
                                        </select>
                                        <br>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="Name">File:</label><br>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="hidden" name="format_surat" id="show_check_format_surat" class="form-control">
                                        <a href="#" data-toggle="modal"
                                        data-target="#modal_show_file_format_surat" class="m-link m-link--brand m--font-bolder"><div id="show_check_format_surat_link">-</div></a>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>


                </div>
                <!-- Modal footer -->
                <div class="modal-footer" style="height: 60px">
                    <button type="submit" class="btn btn-success" id="SubmitCreateForm">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                </div>
            </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="CreateFormatSuratModal">
    <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 85%;overflow-y: initial !important">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header" style="background-color: #782f2ffa;color:white">
                <h6 class="modal-title" style="color: whitesmoke;height:5px;padding-bottom:7px;margin-top:-10px"><b><i class="m-menu__link-icon flaticon-email"></i> Format Surat</b></h6>
                <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body" style="height: 420px;
            overflow-y: auto;">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                    <strong>Success!</strong>Product was added successfully.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <b style="color: red">*Form Format Surat Keluar</b>
                <hr>
                <form id="form_submit_format_surat" method="post">
                    @csrf
                    {{--  <input type="hidden" name="id_suratkel" value="{{$generate}}">  --}}
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4"> <label for="Name">Jenis</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group m-input-group m-input-group--square">
                                        <input type="text" name="jenis_surat" placeholder="Jenis Surat" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="Name">Keterangan:</label>
                                </div>
                                <div class="col-md-8">
                                  <textarea name="keterangan" placeholder="Keterangan Surat" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="Name">Upload File:</label><br>
                                    <small style="color: red"><b>* Format upload .Docx</b></small>
                                </div>
                                <div class="col-md-8">
                                    <input type="file" name="file_upload_format_surat" id="myPdf"/><br>
                                    <canvas style="width: 70%" id="pdfViewer"></canvas>
                                </div>
                            </div>
                        </div>

                    </div>
{{--  datatable  --}}

                </div>

                <hr>
                <table class="table dataTable tabel_show_format_surat" style="width: 100%">
                    <thead style="background-color: #f6f6f6;color:#726363">
                        <tr>
                            <th>No.</th>
                            <th style="width: 25%">Jenis Surat</th>
                            <th style="width: 15%">Keterangan</th>
                            <th>File surat</th>
                            <th style="width: 30%">Aksi</th>

                        </tr>
                    </thead>
                </table>

            </div>
            <!-- Modal footer -->
            <div class="modal-footer" style="height: 60px">
                <button type="submit" class="btn btn-success" id="SubmitCreateForm">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
            </div>
        </form>
        </div>
    </div>
</div>

    {{--  edit data modal  --}}
    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 90%;overflow-y: initial !important">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="background-color: #4f7f4f;color:white">
                    <h6 class="modal-title" style="color: whitesmoke;height:5px;padding-bottom:7px;margin-top:-10px"><b><i class="m-menu__link-icon flaticon-mail"></i> Update Surat Keluar</b></h6>
                    <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" style="height: 420px;
                overflow-y: auto;">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success!</strong>Product was added successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <b style="color: red">*Form Update Surat Keluar</b>
                    <hr>
                    <form id="form_edit" enctype="multipart/form-data" method="post">
                        {!! csrf_field() !!}
                        <input type="hidden" id="txtedit_id_suratkel" name="id_suratkel">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4"> <label for="Name">Tanggal Surat</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="input-group m-input-group m-input-group--square">
                                                <input type="text" name="tgl_suratkel" id="txtedit_tgl_surat" placeholder="Tanggal Surat" class="form-control" autocomplete="off">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Jenis Surat:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="id_jenis" id="txtedit_jenis_surat"
                                            class="custom-select select2 form-control" style="width:100%">
                                            </select>

                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Nomor Surat:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input name="nomor" id="txtedit_nomor_surat"
                                            class="form-control" placeholder="Nomor Surat" style="width:100%">
                                            </input>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Perihal:</label>
                                        </div>
                                        <div class="col-md-8">
                                           <textarea class="form-control" id="txtedit_perihal" name="perihal" placeholder="Perihal..."></textarea>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Kepada:</label>
                                        </div>
                                        <div class="col-md-8">
                                           <input class="form-control" id="txtedit_kepada" name="kepada" placeholder="Kepada..."></input>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-7">

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="Name">Tembusan:</label>
                                        </div>
                                        <div class="col-md-10">
                                            <select name="tembusan[]" placeholder = "tembusan.." id="txtedit_tembusan" multiple="multiple"
                                            class="custom-select select2 form-control" style="width:100%">
                                                @foreach ($dbe as $item)
                                                    <option value="{{$item->id_jabatan}}">{{$item->nama_jabatan}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">File Surat:</label><br>
                                            <small style="color: red"><b>* Format upload JPG atau PDF</b></small>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" readonly class="form-control"  name="file_upload" id="txtedit_file_upload" /><br><br>
                                            {{--  <a id="txtedit_file_surat"></a>  --}}
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>

                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" id="SubmitCreateForm">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                </div>
            </form>
            </div>
        </div>
    </div>


    {{--  modal detail  --}}

    <div class="modal fade" id="modal-detail">
        <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 50%;overflow-y: initial !important">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="background-color: #4f7f4f;color:white">
                    <h6 class="modal-title" style="color: whitesmoke;height:5px;padding-bottom:7px;margin-top:-10px"><b><i class="m-menu__link-icon flaticon-mail"></i> Detail Surat Masuk</b></h6>
                    <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" style="height: 400px;
                overflow-y: auto;">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success!</strong>Product was added successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form id="form_detail" enctype="multipart/form-data" method="post">
                        {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6"> <label for="Name">Tanggal Surat</label>
                                    </div>
                                    <div class="col-md-6">
                                        <b id="detail_tgl_suratkel"></b>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="Name">Jenis Surat</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p id="detail_jenis"></p>

                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="Name">Nomor Surat</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="detail_nomor"></div>

                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="Name">Perihal</label>
                                    </div>
                                    <div class="col-md-6">
                                       <div id="detail_perihal"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="Name">Kepada</label>
                                    </div>
                                    <div class="col-md-6">
                                       <div id="detail_kepada"></div>

                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="Name">File Surat:</label><br>

                                    </div>
                                    <div class="col-md-6">
                                        <a id="detail_file_upload"></a>
                                    </div>
                                </div>
                            </div>

                        </div>

                            <div class="row" style="margin-left:5px">
                                <div class="col-md-2">
                                    <label for="Name">Tembusan</label>
                                </div>
                                <div class="col-md-9">
                                    <select style="height: 5px" disabled placeholder = "tembusan.." multiple="multiple" id="detail_tembusan"
                                        class="custom-select form-control" style="width:100%">
                                            @foreach ($dbe as $item)
                                                <option value="{{$item->id_jabatan}}">{{$item->nama_jabatan}}</option>
                                            @endforeach
                                    </select>


                                </div>
                            </div>


                    </div>


                </div>
                <!-- Modal footer -->
                <div class="modal-footer" style="height: 60px">

                    <button type="button" class="btn btn-danger btn-md m-btn m-btn m-btn--icon" data-dismiss="modal"><span>
                        <i class="la la-close"></i>
                        <span>Keluar</span>
                    </span></button>
                </div>
            </form>
            </div>
        </div>
    </div>


</div>

</div>

<div class="modal fade" id="modal_show_file">
    <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 50%;overflow-y: initial !important">
        <div class="modal-content">
            <!-- Modal Header -->

            <!-- Modal body -->
            <div class="modal-body" style="height: 480px; overflow-y: auto;">

                <embed id="src_img_pdf" type = "application / pdf" width = "100%" height = "450px" />
            </div>
            <!-- Modal footer -->
            <div class="modal-footer" style="height: 60px">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
            </div>
        </form>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_ijazah" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header" style="height:50px">
          <h4 class="modal-title" id="myModalLabel">Berkas surat</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <iframe id="frame_ijazah" width="100%" height="400"></iframe>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="modal-edit-format-surat">
    <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 50%;overflow-y: initial !important">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header" style="background-color: #4f7f4f;color:white">
                <h6 class="modal-title" style="color: whitesmoke;height:5px;padding-bottom:7px;margin-top:-10px"><b><i class="m-menu__link-icon flaticon-mail"></i> Update Surat Keluar</b></h6>
                <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body" style="height: 390px;
            overflow-y: auto;">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                    <strong>Success!</strong>Product was added successfully.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <b style="color: red">*Form Update Surat Keluar</b>
                <hr>
                <form id="form_edit_format_surat" enctype="multipart/form-data" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" id="txteditidformatsurat" name="idformatsurat">
                    <div class="row">
                    <div class="col-md-11">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4"> <label for="Name">Jenis</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group m-input-group m-input-group--square">
                                        <input type="text" name="jenis_surat" id="txteditjenissurat" placeholder="Jenis Surat" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="Name">Keterangan:</label>
                                </div>
                                <div class="col-md-8">
                                  <textarea name="keterangan" id="txteditketerangan" placeholder="Keterangan Surat" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="Name">Upload File:</label><br>
                                    <small style="color: red"><b>* Format upload .Docx</b></small>
                                </div>
                                <div class="col-md-8">
                                    <input type="file" name="file_upload_format_surat" id="myPdf"/><br><br>
                                    <a id="txteditformatsurat"><div id="linkformatsurat"></div></a>
                                </div>
                            </div>
                        </div>


                    </div>

{{--  datatable  --}}

                </div>

            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="SubmitCreateForm">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
            </div>
        </form>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="https://mozilla.github.io/pdf.js/build/pdf.js"></script>

<script>

    $(document).ready(function () {

            $('#input_format_surat').on('change', function(){

            var check = ($(this).val());
            $.ajax({
                type : 'get',
                url  : '{{url("check_format_surat")}}' + '/' + check,
                dataType:'json',
                success:function(response){
                $('#show_check_format_surat').val(response.file_surat);
                $('#show_check_format_surat_link').html(response.file_surat);
                $("#src_file_format_surat").attr("src", 'https://api.aspose.cloud/words/view?foldername=FORMATSURAT&'+'filename='+response.file_surat);

                }
            });

        })
        var pdfjsLib = window['pdfjs-dist/build/pdf'];
// The workerSrc property shall be specified.
        pdfjsLib.GlobalWorkerOptions.workerSrc = 'https://mozilla.github.io/pdf.js/build/pdf.worker.js';

        $("#myPdf").on("change", function(e){
        var file = e.target.files[0]
        if(file.type == "application/pdf"){
		var fileReader = new FileReader();
		fileReader.onload = function() {
			var pdfData = new Uint8Array(this.result);
			// Using DocumentInitParameters object to load binary data.
			var loadingTask = pdfjsLib.getDocument({data: pdfData});
			loadingTask.promise.then(function(pdf) {
			  console.log('PDF loaded');

			  // Fetch the first page
			  var pageNumber = 1;
			  pdf.getPage(pageNumber).then(function(page) {
				console.log('Page loaded');

				var scale = 1.5;
				var viewport = page.getViewport({scale: scale});

				// Prepare canvas using PDF page dimensions
				var canvas = $("#pdfViewer")[0];
				var context = canvas.getContext('2d');
				canvas.height = viewport.height;
				canvas.width = viewport.width;

				// Render PDF page into canvas context
				var renderContext = {
				  canvasContext: context,
				  viewport: viewport
				};
				var renderTask = page.render(renderContext);
				renderTask.promise.then(function () {
				  console.log('Page rendered');
				});
			  });
			}, function (reason) {
			  // PDF loading error
			  console.error(reason);
			});
		};
		fileReader.readAsArrayBuffer(file);
	}
});

        $('#txtedit_tgl_surat').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });
        $("#foto_pegawai").change(function(){
            readURL(this);
        });


        $('#txtinput_tgl_surat_terima').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });

        $('#txtedit_tgl_surat_terima').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });

        $('#txtinput_tgl_surat').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });

        $('#txtedit_tgl_surat').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });

        $('#txtinput_jenis_surat').select2({
            placeholder: 'Jenis Surat...',
            dropdownParent: $("#txtinput_jenis_surat").parent(),
            allowClear: true,
            ajax: {
              url: '{{url("/cari/jenis_surat")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nama,
                      id: item.id_jenis
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#txtedit_jenis_surat').select2({
            placeholder: 'Jenis Surat...',
            dropdownParent: $("#txtedit_jenis_surat").parent(),
            allowClear: true,
            ajax: {
              url: '{{url("/cari/jenis_surat")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nama,
                      id: item.id_jenis
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#txtinput_tembusan').select2({
            placeholder: 'Jabatan...',
            dropdownParent: $("#txtinput_tembusan").parent(),
            allowClear: true,
            ajax: {
              url: '{{url("/cari/jabatanwithauth")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nama_jabatan,
                      id: item.id_jabatan
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#txtedit_tembusan').select2({
            placeholder: 'Jabatan...',
            dropdownParent: $("#txtedit_tembusan").parent(),
            allowClear: true,
            ajax: {
              url: '{{url("/cari/jabatanwithauth")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nama_jabatan,
                      id: item.id_jabatan
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#detail_tembusan').select2({
            placeholder: 'Jabatan...',
            dropdownParent: $("#detail_tembusan").parent(),
            ajax: {
              url: '{{url("/cari/jabatanwithauth")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nama_jabatan,
                      id: item.id_jabatan
                    }
                  })
                };
              },
              cache: true,
            }
          });


          $('#txtinput_id_kode_arsip').select2({
            placeholder: 'Kode Arsip...',
            dropdownParent: $("#txtinput_id_kode_arsip").parent(),
            ajax: {
              url: '{{url("/cari/kode_arsip")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.kode_arsip + ' - ' + item.keterangan,
                      id: item.id_kode_arsip
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#txtedit_id_kode_arsip').select2({
            placeholder: 'Kode Arsip...',
            dropdownParent: $("#txtedit_id_kode_arsip").parent(),
            ajax: {
              url: '{{url("/cari/kode_arsip")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.kode_arsip + ' - ' + item.keterangan,
                      id: item.id_kode_arsip
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#txtedit_id_jabatan').select2({
            placeholder: 'Jabatan...',
            dropdownParent: $("#txtedit_id_jabatan").parent(),
            ajax: {
              url: '{{url("/cari/jabatan")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nama_jabatan,
                      id: item.id_jabatan
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#txtinput_kategori_surat').select2({
            placeholder: 'Kategori Surat...',
            dropdownParent: $("#txtinput_kategori_surat").parent(),
            ajax: {
              url: '{{url("/cari/kategori_surat")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.kategori,
                      id: item.id_kategori
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#txtedit_kategori_surat').select2({
            placeholder: 'Kategori Surat...',
            dropdownParent: $("#txtedit_kategori_surat").parent(),
            ajax: {
              url: '{{url("/cari/kategori_surat")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.kategori,
                      id: item.id_kategori
                    }
                  })
                };
              },
              cache: true,
            }
          });

          $('#input_format_surat').select2({
            placeholder: 'Pilih Format Surat...',
            dropdownParent: $("#input_format_surat").parent(),
            ajax: {
              url: '{{url("/cari/formatsurat")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.jenis_surat,
                      id: item.id
                    }
                  })
                };
              },
              cache: true,
            }
          });

        var table_surat = $('.tabel_show').DataTable({
            responsive:true,
            paging: true,
            info: true,
            searching: true,
            "aaSorting": [],
            "ordering": true,
            ajax: {
                url: '{{url("show_datatable_surat_keluar")}}',
                dataSrc: 'result',
            },

        });

        var table_surat_format = $('.tabel_show_format_surat').DataTable({
            responsive:true,
            paging: true,
            info: true,
            searching: true,
            "aaSorting": [],
            "ordering": true,
            ajax: {
                url: '{{url("show_datatable_format_surat")}}',
                dataSrc: 'result',
            },

        });

        //input data

        $('#form_submit').submit(function (event) {
            event.preventDefault();
            $('#CreateSuratModal').modal('hide');
            var url = '{{ url('simpan_surat_keluar') }}';
            ajaxProcessInsert(url, new FormData(this))
        });

        function ajaxProcessInsert(url, data) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    if (response) {
                        console.log(response);
                        $('.cssload-container').hide();
                        $('#form_submit')[0].reset();
                        $('.tabel_show').DataTable().ajax.reload();
                        Swal.fire({
                            title: 'Information',
                            html: 'Data berhasil disimpan',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.errors;
                            if (Array.isArray(message)) {
                                console.log(message);
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };

        //action edit

        $('#form_edit').submit(function (event) {
            $('#modal-edit').modal('hide');
            event.preventDefault();
            var url = '{{ url('update_surat_keluar') }}';
            ajaxProcess(url, new FormData(this));
        });

        function ajaxProcess(url, data) {
            $.ajax({
                type: 'post',
                url: url,
                data: data,
                dataType : 'json',
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    console.log(response);
                    if (response) {
                        $('.cssload-container').hide();

                        $('.tabel_show').DataTable().ajax.reload();


                        Swal.fire({
                            title: 'Information',
                            html: '<strong>' + response.result + '</strong>',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('#form_edit')[0].reset();
                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };

        $('.tabel_show tbody').on('click', '#btn_update_surat', function (e) {
            var suratid = $(this).attr('data-suratid');
            var url = '{!! url('get_surat_keluar') !!}/'+suratid;

            ajaxEditSurat(url);
        });

        function ajaxEditSurat(url){
            $.ajax({
                type: 'GET',
                url: url,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    $('.cssload-container').hide();
                    if(response.status == 'success') {
                       if(response.result != null){
                           var data = response.result;
                           $('#txtedit_id_suratkel').val(data.id_suratkel);
                           $('#txtedit_nomor_surat').val(data.nomor);
                           $('#txtedit_perihal').val(data.perihal);
                           $('#txtedit_kepada').val(data.kepada);
                           $('#txtedit_file_upload').val(data.file_upload);

                           var tgl_surat_reverse = data.tgl_suratkel;
                           date_tgl_surat = tgl_surat_reverse.split("-").reverse().join("-");
                           $('#txtedit_tgl_surat').datepicker('setDate', date_tgl_surat);
                           console.log(data.tembusan);
                           var arr = data.tembusan;
                           if(arr != null){
                                var explode = arr.split(",", 10);
                                $('#txtedit_tembusan').val(explode).trigger('change');
                           }else{
                               arr = null;
                           }
                           $('#txtedit_file_surat').html(data.file_upload);
                           $('#txtedit_file_surat').attr('href', 'FILESURATKELUAR/' + data.file_upload);
                           var selected_jenis_surat = $("<option selected='selected'></option>").val(data.id_jenis).text(data.nama);
                           $("#txtedit_jenis_surat").append(selected_jenis_surat).trigger('change');

                           $('#modal-edit').modal('show');
                       }
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        }

        //modal detail jquery

        $('.tabel_show tbody').on('click', '#btn_detail_surat', function (e) {
            var suratid = $(this).attr('data-suratiddetail');
            var url = '{!! url('get_surat_keluar') !!}/'+suratid;

            ajaxDetailSurat(url);
        });

        function ajaxDetailSurat(url){
            $.ajax({
                type: 'GET',
                url: url,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    $('.cssload-container').hide();
                    if(response.status == 'success') {
                       if(response.result != null){
                           var data = response.result;
                           $('#detail_id_surat').html(':  '+data.id_suratkel);
                           $('#detail_tgl_suratkel').html(':  '+data.tgl_suratkel);
                           $('#detail_nomor').html(':  '+data.nomor);
                           $('#detail_jenis').html(':  '+data.nama);
                           $('#detail_perihal').html(':  '+data.perihal);
                           $('#detail_kepada').html(':  '+data.kepada);
                           $("#detail_file_upload").html(':  '+data.file_upload);
                           $("#detail_file_upload").attr("href", 'FILESURATKELUAR'+'/'+data.file_upload);
                           var arr = data.tembusan;
                           if(arr != null){
                                var explode = arr.split(",", 10);
                                $('#detail_tembusan').val(explode).trigger('change');
                           }else{
                               arr = null;
                           }
                       }
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        }


        //delete data propinsi

        $('.tabel_show tbody').on('click', '#btn-delete-surat-masuk', function () {
            var url = '{{ url('destroy_surat_keluar') }}';
            var id = $(this).attr('data-id');
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var data_id = {_token: CSRF_TOKEN, id: id};
            delete_master(url, data_id);
        });


        function delete_master(url, data) {
            swal.fire({
                title: "Informasi !",
                text: "Apakah anda ingin menghapus data ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Ya, Hapus data!'
            }).then((result) => {
                if(result.value)
                {
                    $.ajax({
                    type: "POST",
                    url: url,
                    /* send the csrf-token and the input to the controller */
                    data: data,
                    dataType: "json",
                    /* remind that 'data' is the response of the AjaxController */
                    beforeSend: function () {
                        $('.cssload-container').show();
                    },
                    success: function (data) {
                        $('.cssload-container').hide();
                        $('.tabel_show').DataTable().ajax.reload();

                        swal.fire({
                            title: "Delete!",
                            text: data.result,
                            type: data.status,
                            button: false,
                            timer: 2000,
                        })
                    },
                    error: function (error) {
                        if (error) {
                            console.log(error);
                            var result = error.responseJSON;
                            if (result != null) {
                                var message = result.message;
                                if (Array.isArray(message)) {
                                    $.map(message, function (value, index) {
                                        message = value + '</br>';
                                    });
                                }
                            } else {
                                message = 'look like something when wrong';
                            }
                        } else {
                            message = 'look like something when wrong';
                        }

                        $('.cssload-container').hide();
                        Swal.fire({
                            title: 'Warning',
                            html: '<strong>' + message + '</strong>',
                            type: 'warning',
                            showConfirmButton: false,
                            timer: 3000
                        })

                    }

                });
                }
            });
        }


        //format surat

        $('#form_submit_format_surat').submit(function (event) {
            event.preventDefault();
            {{--  $('#CreateFormatSuratModal').modal('hide');  --}}
            var url = '{{ url('simpan_format_surat') }}';
            ajaxProcessInsertFormatSurat(url, new FormData(this))
        });

        function ajaxProcessInsertFormatSurat(url, data) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    if (response) {
                        console.log(response);
                        $('.cssload-container').hide();
                        $('#form_submit_format_surat')[0].reset();
                        $('.tabel_show_format_surat').DataTable().ajax.reload();
                        Swal.fire({
                            title: 'Information',
                            html: 'Data berhasil disimpan',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.errors;
                            if (Array.isArray(message)) {
                                console.log(message);
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };

        $('.tabel_show_format_surat tbody').on('click', '#btn_update_format_surat', function (e) {
            var suratid = $(this).attr('data-id');
            var url = '{!! url('get_format_surat') !!}/'+suratid;

            ajaxEditFormatSurat(url);
        });

        function ajaxEditFormatSurat(url){
            $.ajax({
                type: 'GET',
                url: url,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    $('.cssload-container').hide();
                    if(response.status == 'success') {
                       if(response.result != null){
                           var data = response.result;
                           console.log(data);
                           $('#txteditidformatsurat').val(data.id);
                           $('#txteditjenissurat').val(data.jenis_surat);
                           $('#txteditketerangan').val(data.keterangan);
                           $('#txteditformatsurat').attr('href', 'FORMATSURAT/' + data.file_surat);
                           $('#linkformatsurat').html(data.file_surat);

                           $('#modal-edit-format-surat').modal('show');
                       }
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        console.log(result);
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wronsadg';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        }

        $('#form_edit_format_surat').submit(function (event) {
            $('#modal-edit-format-surat').modal('hide');
            event.preventDefault();
            var url = '{{ url('update_format_surat') }}';
            ajaxProcessEditFormatSurat(url, new FormData(this));
        });

        function ajaxProcessEditFormatSurat(url, data) {
            $.ajax({
                type: 'post',
                url: url,
                data: data,
                dataType : 'json',
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    console.log(response);
                    if (response) {
                        $('.cssload-container').hide();

                        $('.tabel_show_format_surat').DataTable().ajax.reload();


                        Swal.fire({
                            title: 'Information',
                            html: '<strong>' + response.result + '</strong>',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('#form_edit')[0].reset();
                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };

        $('.tabel_show_format_surat tbody').on('click', '#btn-delete-format-surat', function () {
            var url = '{{ url('destroy_format_surat') }}';
            var id = $(this).attr('data-id');
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var data_id = {_token: CSRF_TOKEN, id: id};
            delete_master_format_surat(url, data_id);
        });


        function delete_master_format_surat(url, data) {
            swal.fire({
                title: "Informasi !",
                text: "Apakah anda ingin menghapus format surat ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Ya, Hapus data!'
            }).then((result) => {
                if(result.value)
                {
                    $.ajax({
                    type: "POST",
                    url: url,
                    /* send the csrf-token and the input to the controller */
                    data: data,
                    dataType: "json",
                    /* remind that 'data' is the response of the AjaxController */
                    beforeSend: function () {
                        $('.cssload-container').show();
                    },
                    success: function (data) {
                        $('.cssload-container').hide();
                        $('.tabel_show_format_surat').DataTable().ajax.reload();

                        swal.fire({
                            title: "Delete!",
                            text: data.result,
                            type: data.status,
                            button: false,
                            timer: 2000,
                        })
                    },
                    error: function (error) {
                        if (error) {
                            console.log(error);
                            var result = error.responseJSON;
                            if (result != null) {
                                var message = result.message;
                                if (Array.isArray(message)) {
                                    $.map(message, function (value, index) {
                                        message = value + '</br>';
                                    });
                                }
                            } else {
                                message = 'look like something when wrong';
                            }
                        } else {
                            message = 'look like something when wrong';
                        }

                        $('.cssload-container').hide();
                        Swal.fire({
                            title: 'Warning',
                            html: '<strong>' + message + '</strong>',
                            type: 'warning',
                            showConfirmButton: false,
                            timer: 3000
                        })

                    }

                });
                }
            });
        }

    })
</script>

@endsection
