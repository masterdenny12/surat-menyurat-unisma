@extends('master')
@section('konten')
<div class="m-portlet m-portlet--mobile" style="margin: -15px;margin-top:-35px">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <i class="m-menu__link-icon flaticon-list"></i>&nbsp  Role Management
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <div class="pull-right">

                        <a class="btn btn-success" href="{{ route('roles.create') }}"> Create New Role</a>

                    </div>
                <li class="m-portlet__nav-item"></li>
                <li class="m-portlet__nav-item">
                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                        <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                            <i class="la la-ellipsis-h m--font-brand"></i>
                        </a>

                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="row">

            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                </div>
            </div>
        </div>

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif


        <table class="ui celled table table-hover dataTable no-footer tabel_show" >
            <thead style="background-color: #f6f6f6;color:#726363">
        <tr>
            <th>No.</th>
            <th>Name</th>
            <th>Update at</th>
            <th>Aksi</th>

        </tr>
    </thead>

</table>

    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script>
    $(document).ready(function(){

        var table_users = $('.tabel_show').DataTable({
            responsive:true,
            paging: true,
            info: true,
            searching: true,
            "aaSorting": [],
            "ordering": true,
            ajax: {
                url: '{{url("datatable_roles")}}',
                dataSrc: 'result',
            },

        });
    });
</script>
@endsection
