@extends('master_sub')

@section('konten')

<div class="row">

    <div class="col-xl-5 col-lg-12">

        <!--Begin::Portlet-->
        <div class="m-portlet m-portlet--full-height ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Detail Surat
                        </h3>
                    </div>
                </div>

            </div>
            <div class="m-portlet__body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">Tanggal Surat</label>
                        </div>
                        <div class="col-md-7">
                           <label>: <mark><b>{{$get_surat->tgl_surat}}</b></mark></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">Kategori Surat</label>
                        </div>
                        <div class="col-md-7">
                           <label>: {{$get_surat->kategori}}</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">Jenis Surat</label>
                        </div>
                        <div class="col-md-7">
                           <label>: <b>{{$get_surat->nama}}</b></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">Nomor Surat</label>
                        </div>
                        <div class="col-md-7">
                           <label>: {{$get_surat->nomor_surat}}</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">Perihal</label>
                        </div>
                        <div class="col-md-7">
                           <label>: {{$get_surat->perihal}}</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">Dari</label>
                        </div>
                        <div class="col-md-7">
                           <label>: <b>{{$get_surat->dari}}</b></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">Kepada</label>
                        </div>
                        <div class="col-md-7">
                           <label>: {{$get_surat->nama_jabatan}}</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="Name">File Surat</label>
                        </div>
                        <div class="col-md-7">
                            : <a href="#" data-toggle="modal"
                            data-target="#modal_show_file" class="m-link m-link--brand m--font-bolder">{{$get_surat->file_upload}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--End::Portlet-->
    </div>
    <div class="col-xl-7 col-lg-12">

        <!--Begin::Portlet-->
        <div class="m-portlet  m-portlet--full-height ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Disposisi Surat
                        </h3>
                    </div>
                </div>

                @if (Auth::user()->role_id == 2)
                    @if ($count_disposisi == 0)
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="#" data-toggle="modal" data-target="#modal_tambah_disposisi" class="btn btn-primary m-btn m-btn--icon m-btn--air">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>Tambah Disposisi</span>
                                    </span>
                                </a>

                            </li>
                        </ul>
                    </div>
                    @else
                    @php
                        echo '';
                    @endphp
                    @endif
                @else
                    @php
                        echo '';
                    @endphp
                @endif

            </div>
            <div class="m-portlet__body">
                <div class="m-scrollable m-scroller ps ps--active-y" data-scrollable="true" data-height="330" data-mobile-height="300" style="height: 200px; overflow: hidden;">

                    <!--Begin::Timeline 2 -->
                    @if ($count_query > 0)
                        @foreach ($query as $item)
                            @php
                                $time_disposisi = date("h:i", strtotime($item->jam_disposisi ));
                            @endphp
                            <div class="m-timeline-2">
                                <div  class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30">
                                    <div class="m-timeline-2__item"  >
                                        <span class="m-timeline-2__item-time" >{{$time_disposisi}}</span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i class="fa fa-genderless m--font-danger"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text  m--padding-top-2">

                                            <b class="text-right">{{$item->tgl_disposisi}}</b><br>
                                            Disposisi ke : <b style="color: steelblue">{{$item->nama_jabatan}}</b><br><hr style="width: 400px">
                                            <b>Catatan :</b> {{$item->isi_disposisi}}<br>
                                            <mark style="background-color:#f2f2f2b5"><small>Oleh : <b style="color: rgb(175, 82, 102)">{{$item->name}}</b></small></mark>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                            <div>Belum ada disposisi surat...</div>
                    @endif


                    <!--End::Timeline 2 -->
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 380px; right: 4px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 215px;"></div></div></div>
            </div>
        </div>

        <!--End::Portlet-->
    </div>

</div>


<div class="modal fade" id="modal_show_file">
    <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 50%;overflow-y: initial !important">
        <div class="modal-content">
            <!-- Modal Header -->

            <!-- Modal body -->
            <div class="modal-body" style="height: 480px; overflow-y: auto;">
                @php
                $cek = "FILEUPLOAD/".$get_surat->file_upload;
                @endphp
                <embed src = "{{url($cek) }}" type = "application / pdf" width = "100%" height = "450px" />
            </div>
            <!-- Modal footer -->
            <div class="modal-footer" style="height: 60px">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
            </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_tambah_disposisi">
    <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 70%;overflow-y: initial !important">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header" style="background-color: #4f7f4f;color:white">
                <h6 class="modal-title" style="color: whitesmoke;height:1px;padding-bottom:5px;margin-top:-10px"><b><i class="m-menu__link-icon la la-send"></i> Kirim Disposisi Surat</b></h6>
                <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body" style="height: 430px; overflow-y: auto;">
                <form id="form_submit" method="post">
                    @csrf
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">
                            <label for="Name">Kepada</label>
                        </div>
                        <div class="col-md-10">
                            <input type="hidden" name="id_surat" value="{{$get_surat->id_surat}}">
                            <select name="id_jabatan[]" placeholder = "tembusan.." id="txtinput_id_jabatan" multiple="multiple"
                            class="custom-select select2 form-control" style="width:100%">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">
                            <label for="Name">Opsi</label>
                        </div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Mohon Pertimbangan" > Mohon Pertimbangan
                                        <span></span>
                                    </label>
                                    <br>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Mohon Pendapat"> Mohon Pendapat
                                        <span></span>
                                    </label><br>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Mohon Keputusan"> Mohon Keputusan
                                        <span></span>
                                    </label><br>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Mohon Petunjuk"> Mohon Petunjuk
                                        <span></span>
                                    </label><br>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Mohon Saran"> Mohon Saran
                                        <span></span>
                                    </label><br>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Bicarakan"> Bicarakan
                                        <span></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Teliti dan ikuti perkembangannya"> Teliti dan ikuti perkembangannya
                                        <span></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Untuk perhatian"> Untuk perhatian
                                        <span></span>
                                    </label><br>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Siapkan konsep"> Siapkan konsep
                                        <span></span>
                                    </label>

                                </div>
                                <div class="col-md-6">
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Siapkan Laporan"> Siapkan Laporan
                                        <span></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Siapkan dasar peraturan"> Siapkan dasar peraturan
                                        <span></span>
                                    </label>
                                    <br>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Untuk diproses"> Untuk diproses
                                        <span></span>
                                    </label><br>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Selesaikan sesuai dengan pembicaraan"> Selesaikan sesuai dengan pembicaraan
                                        <span></span>
                                    </label><br>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="edarkan ke unit-unit terkait"> edarkan ke unit-unit terkait
                                        <span></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Tik / gandakan"> Tik / gandakan
                                        <span></span>
                                    </label>
                                    <br>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Wakili"> Wakili
                                        <span></span>
                                    </label>
                                    <br>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--success">
                                        <input type="checkbox" name="opsi[]" value="Arsipkan"> Arsipkan
                                        <span></span>
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">
                            <label for="Name">Disposisi <br><small style="color: red"><i>Catatan</i></small></label>
                        </div>
                        <div class="col-md-10">
                           <textarea class="form-control" name="isi_disposisi" placeholder="Isi Disposisi..." rows="1"></textarea>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer" style="height: 60px">
                <button type="submit" class="btn btn-success">Kirim Disposisi</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
            </div>
        </form>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script>
    $(document).ready(function () {
        $('#txtinput_id_jabatan').select2({
            placeholder: 'Jabatan...',
            dropdownParent: $("#txtinput_id_jabatan").parent(),
            allowClear: true,
            ajax: {
            url: '{{url("/cari/jabatanwithauth")}}',
            dataType: 'json',
            delay: 50,
            processResults: function (data) {
                return {
                results:  $.map(data, function (item) {
                    return {
                    text: item.nama_jabatan,
                    id: item.id_jabatan
                    }
                })
                };
            },
            cache: true,
            }
        });

        $('#form_submit').submit(function (event) {
            event.preventDefault();
            $('#modal_tambah_disposisi').modal('hide');
            var url = '{{ url('kirim_disposisi') }}';
            ajaxProcessInsert(url, new FormData(this))
        });

        function ajaxProcessInsert(url, data) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    if (response) {
                        console.log(response);
                        $('.cssload-container').hide();
                        $('#form_submit')[0].reset();
                        $('.tabel_show').DataTable().ajax.reload();
                        Swal.fire({
                            title: 'Information',
                            html: 'Surat berhasil dikirm',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        });
                        location.reload();

                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.errors;
                            if (Array.isArray(message)) {
                                console.log(message);
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };

    });
</script>
@endsection
