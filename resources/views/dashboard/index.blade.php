@extends('master')
@section('konten')

<div class="m-grid__item m-grid__item--fluid m-wrapper" style="margin:10px">
<div class="m-portlet ">
							<div class="m-portlet__body  m-portlet__body--no-padding">
								<div class="row m-row--no-padding m-row--col-separator-xl">
                                    @php
                                        $check_ukuran = '';
                                        $check_ukuran = Auth::user()->role_id == 1 ||  Auth::user()->role_id == 2 ? 'col-md-12 col-lg-6 col-xl-3' : 'col-md-12 col-lg-6 col-xl-6' ;
                                    @endphp

									<div class="{{$check_ukuran}}">

										<!--begin::Total Profit-->
                                        <div class="m-widget24">
											<div class="m-widget24__item">
												<h4 class="m-widget24__title">
													Surat
												</h4><br>
												<span class="m-widget24__desc">
													Total Surat Masuk
												</span>
												<span class="m-widget24__stats m--font-brand">
													{{$data[0]['total_surat_masuk']}}
												</span>
												<div class="m--space-10"></div>
												<div class="progress m-progress--sm">
													<div class="progress-bar m--bg-brand" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<span class="m-widget24__change">
													Preview
												</span>

											</div>
										</div>

										<!--end::Total Profit-->
									</div>
									<div class="{{$check_ukuran}}">

										<!--begin::New Feedbacks-->
                                        <div class="m-widget24">
											<div class="m-widget24__item">
												<h4 class="m-widget24__title">
													Surat
												</h4><br>
												<span class="m-widget24__desc">
													Total Surat Keluar
												</span>
												<span class="m-widget24__stats m--font-danger">
													{{$data[0]['total_surat_keluar']}}
												</span>
												<div class="m--space-10"></div>
												<div class="progress m-progress--sm">
													<div class="progress-bar m--bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<span class="m-widget24__change">
													Preview
												</span>

											</div>
										</div>
										<!--end::New Feedbacks-->
                                    </div>
                                    @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                                        <div class="col-md-12 col-lg-6 col-xl-3">

                                            <!--begin::New Orders-->
                                            <div class="m-widget24">
                                                <div class="m-widget24__item">
                                                    <h4 class="m-widget24__title">
                                                        Surat
                                                    </h4><br>
                                                    <span class="m-widget24__desc">
                                                        Total Disposisi Surat
                                                    </span>
                                                    <span class="m-widget24__stats m--font-success">
                                                        {{$data[0]['total_surat_disposisi']}}
                                                    </span>
                                                    <div class="m--space-10"></div>
                                                    <div class="progress m-progress--sm">
                                                        <div class="progress-bar m--bg-success" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span class="m-widget24__change">
                                                        Preview
                                                    </span>

                                                </div>
                                            </div>

                                            <!--end::New Orders-->
                                        </div>
                                        <div class="col-md-12 col-lg-6 col-xl-3">

                                            <!--begin::New Users-->
                                            <div class="m-widget24">
                                                <div class="m-widget24__item">
                                                    <h4 class="m-widget24__title">
                                                        Surat
                                                    </h4><br>
                                                    <span class="m-widget24__desc">
                                                        Monitoring Surat
                                                    </span>
                                                    <span class="m-widget24__stats m--font-warning">
                                                        {{$data[0]['count_monitoring']}}
                                                    </span>
                                                    <div class="m--space-10"></div>
                                                    <div class="progress m-progress--sm">
                                                        <div class="progress-bar m--bg-warning" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span class="m-widget24__change">
                                                        Preview
                                                    </span>

                                                </div>
                                            </div>

                                            <!--end::New Users-->
                                        </div>
                                    @endif

								</div>
							</div>
</div>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <i class="m-menu__link-icon flaticon-graph"></i>&nbsp Chart Surat Masuk
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <select id="change_surat_masuk"
                    class="custom-select select2 form-control" style="width:100%">
                    {{--  {{dd($get_tahun)}}  --}}
                    <?php
                        $year = date('Y');
                        $selected = '';
                    ?>
                    <?php foreach ($tahun as $key => $value) {

                        if($year == $value->tahun){
                          $selected = 'selected';
                          echo '<option '.$selected.' value="'.$value->tahun.'"  data-tahun="'.$value->tahun.'"> '.$value->tahun.'</option>';
                        }else{
                          echo '<option value="'.$value->tahun.'" data-tahun="'.$value->tahun.'"> '.$value->tahun.'</option>';
                        }

                      } ?>


                    </select>

                </li>
            </ul>
        </div>
    </div>

        <div class="col-md-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="m-widget14">
                <div class="m-widget14__header m--margin-bottom-30">
                    <h3 class="m-widget14__title">
                    </h3>
                </div>
                <div id="chart_surat_masuk"></div>
            </div>
        </div>
</div>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <i class="m-menu__link-icon flaticon-graph"></i>&nbsp Chart Surat Keluar
                </h3>
            </div>
        </div>

    </div>

        <div class="col-md-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="m-widget14">
                <div class="m-widget14__header m--margin-bottom-30">
                    <h3 class="m-widget14__title">
                    </h3>
                </div>
                <div id="chart_surat_keluar"></div>
            </div>
        </div>
</div>

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="https://code.highcharts.com/highcharts.src.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script>
    $(document).ready(function () {
        $('#change_surat_masuk').on('change', function(){
            var tahun = $(this).val();
            console.log(tahun);
            chart_masuk(tahun);
        });

        var d = new Date();
        var n = d.getFullYear();
        chart_masuk(n);
        chart_keluar(n);

    function chart_masuk(tahun){

        $.ajax({
            url: "{{url('graph_surat_masuk')}}" + '/' +tahun,
            type: 'GET',
            dataType: 'JSON',
            success:function(response){
                // console.log(response.bulan);
            Highcharts.chart('chart_surat_masuk', {
                    chart: {
                    type: 'column'
                    },
                    credits: {
                            text: '',
                        },
                    title: {
                        text: 'Total Surat masuk/bulan'
                    },
                    xAxis: {
                        categories: response.bulan
                    },

                    yAxis: {
                        title: {
                            text: 'Surat Masuk'
                        }
                    },
                    colors : ['#00a65a'],
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: true
                            }

                        }
                    },

                    series: [{
                        name: 'Surat Masuk',
                        data: response.jumlah
                    }],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },

                        }]
                    }

                });
            }
        });
    }

    function chart_keluar(tahun){

        $.ajax({
            url: "{{url('graph_surat_keluar')}}" + '/' +tahun,
            type: 'GET',
            dataType: 'JSON',
            success:function(response){
                // console.log(response.bulan);
            Highcharts.chart('chart_surat_keluar', {
                    chart: {
                    type: 'column'
                    },
                    credits: {
                            text: '',
                        },
                    title: {
                        text: 'Total Surat Keluar/bulan'
                    },
                    xAxis: {
                        categories: response.bulan
                    },

                    yAxis: {
                        title: {
                            text: 'Surat Keluar'
                        }
                    },
                    colors : ['red'],
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: true
                            }

                        }
                    },

                    series: [{
                        name: 'Surat Keluar',
                        data: response.jumlah
                    }],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },

                        }]
                    }

                });
            }
        });
    }
    });
</script>

@endsection
