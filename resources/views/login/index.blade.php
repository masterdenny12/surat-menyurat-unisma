<html>
	<head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet" type="text/css" />
        <style>

.input-field label{color:#999;}
.input-field input[type=text]:focus + label,.input-field input[type=password]:focus + label{color:#000;}
.input-field input[type=text]:focus ,.input-field input[type=password]:focus {border-bottom:1px solid #000;box-shadow:0 1px 0 0 #000;}
.input-field input[type=text].valid,.input-field input[type=password].valid{border-bottom:1px solid #2196F3;box-shadow:0 1px 0 0 #2196F3;}
.input-field input[type=text].invalid,.input-field input[type=password].invalid{border-bottom:1px solid #F44336;box-shadow:0 1px 0 0 #F44336;}
.input-field .prefix.active{color:#000;}
/* Input : switch */
.switch{margin-top:7px;}
.switch label .lever{margin:0 7px;}
.switch label input[type="checkbox"]:checked + .lever{background-color:#ADD0EB;}
.switch label input[type="checkbox"]:checked + .lever::after{background-color: #4FB0FD ;}
.login-body{background-color:#42784a;background-size:cover;background-position: center;background-attachment:fixed;}
.input-cart{border-radius:3px;min-height:200px;border-top:1px solid #13B916;border-bottom:5px solid #31D734;border-left:7px solid #D0F2EC;margin-top:45px;margin-bottom:0px;}
.login{margin-top:25px;border-right:1px solid #ddd;}
.policy{visibility: hidden;}
.signupForm{display: none;}
.signup{margin-top:25px;}
.signup-toggle{cursor:pointer;margin-top:140px;}
.login h4, .signup h4{font-weight:200;}
.legal{border-top:1px solid #ddd;}
.email label{margin-left:11px;}
.policy{font-size:13px;}
.main-title{font-family:pacifico;}
.msg {
    width:100%;
    border: 1px solid;
    padding:10px;
    margin: 20px;
    color: grey;
  }
  .msg-error {
    // rouge
    border-color: #d32f2f;
    background-color: #ef5350;
    color: white;
    margin-left:-5px;
    margin-bottom:30px;
    border-radius:5px;
  }
  .msg-error{
      display: none;
  }
  .indeterminate{
    display: none;
  }
        </style>
		<meta charset="utf-8">
		<title>Login</title>
    </head>

	<body class="login-body">
		<div class="row">

			<div class="input-cart col s12 m8 push-m2 z-depth-2 white lighten-1">
                <div class="progress" style="background-color: white">
                    <div class="indeterminate"></div>
                </div>

				<div class="col s12 m5 login">

					<h5 class="center">Login Area</h5>
                    <br>


                        <div class="msg msg-error z-depth-6 scale-transition" style="padding-right: 12px">
                            Username / Password Salah
                        </div>

						<div class="row">
							<div class="input-field" style="color: green">
								<input type="text" id="user" name="username" id="username" class="validate" required="required" placeholder="Username">
                <label for="user">
                  <i class="material-icons" style="margin-top: -28px;">person</i>                </label>
							</div>
						</div>
						<div class="row">
							<div class="input-field">
								<input type="password" id="pass" name="password" class="validate" required="required" placeholder="Password">
								<label for="pass">
                <i class="material-icons" style="margin-top: -28px;">lock</i>
                </label>
							</div>
						</div>
						<div class="row">
							<div class="switch col s6">

							</div>
							<div class="col s6">
								<button type="button" name="login" id="SubmitLogin" class="btn waves-effect waves-light green right">Login</button>
							</div>
						</div>

				</div>
				<!-- Signup form -->
				<div class="col s12 m7 signup">
				<div class="signupForm">
					<h4 class="center">Sign up</h4>
					<br>
					<form action="regCheck.php" name="signup" method="post" autocomplete="off">
						<div class="row">
							<div class="input-field col s12 m6">
								<input type="text" id="name-picked" name="namepicked" class="validate" required="required" placeholder="Enter a username">
								<label for="name-picked">
                       <i class="material-icons">person_add</i>
                </label>
							</div>
							<div class="input-field col s12 m6">
								<input type="password" id="pass-picked" name="passpicked" class="validate" required="required" placeholder="Password">
								<label for="pass-picked">
                  <i class="material-icons">lock</i>                    </label>
							</div>
						</div>
						<div class="row">
							<div class="input-field email">
								<div class="col s12">
									<input type="text" id="email" name="email" class="validate" required="required" placeholder="Enter your email">
									<label for="email">
                    <i class="material-icons">mail</i>
                  </label>
								</div>
							</div>
						</div>
					</form>
					<div class="row">
						<button type="submit" name="btn-signup" class="btn blue right waves-effect waves-light">Sign Up</button>
					</div>
					</div>
					<div class="signup-toggle center" style="margin-top: 70px">
                        <img src="{{url('images/logounisma.png')}}" width="25%">
                        <h4 class="center">Sistem Informasi</h4>
                        <h5 class="center">Surat Menyurat<small style="color: grey">  v.1.0</small></h5>
                        <p class="center grey-text" style="font-size: 16px;">Published By : <a href="http://fb.com/celyes01" class="main-title green-text" target="_blank">Universitas Islam Malang</a></p>
					</div>
				</div>
				<div class="col s12">
					<br>
          <div class="legal center">
					</div>
					<div class="legal center">
					<div class="col s12 m7 right">
						<p class="grey-text policy center">By signing up, you agree on our <a href="#!">Privacy Policy</a> and  <a href="#!">Terms of Use</a> including <a href="#!">Cookie Use</a>.</p>
					</div>
					<div class="col s12 m5">
						<p class="left grey-text" style="font-size: 14px;">Informasi : <a href="http://fb.com/celyes01" class="main-title blue-text" target="_blank">Petunjuk login</a></p>
					</div>
					</div>

				</div>
			</div>
		</div>

	</body>
</html>
{{--  <script src="assets/vendors/jquery/dist/jquery.js" type="text/javascript"></script>  --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>

jQuery(document).ready(function($){

    $('.msg-error').hide();
    $('.indeterminate').hide();


    $('#SubmitLogin').click(function(e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{url('post-login')}}",
            method: 'post',
            data: {
                username: $('#user').val(),
                password: $('#pass').val(),

            },
            success: function(result) {
                console.log(result);
                if(result.errors) {

                    $('.indeterminate').show();
                    setTimeout(function(){
                        $('.msg-error').show();
                     }, 1000);
                    setTimeout(function(){
                        $('.indeterminate').hide();
                     }, 1000);

                } else if(result.success){
                    $('.msg-error').hide();
                    $('.indeterminate').show();
                    setTimeout(function(){
                        window.location.href = "{{url('dashboard')}}";
                        $('.indeterminate').hide();
                     }, 2000);


                }
            }
        });
    });

    $("#pass").keyup(function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("SubmitLogin").click();
        }
    });

});


</script>
